#ifndef GVK_IMGUI_RENDERER_H
#define GVK_IMGUI_RENDERER_H

#include <vulkan/vulkan.h>
#include <vector>
#include <cstdint>
#include <exception>
#include <stdexcept>
#include <cassert>

#include <vkb/utils/DynamicPipeline.h>
#include <vkb/utils/DescriptorSetAllocator.h>
#include <vkb/utils/DescriptorUpdater.h>
#include <vkb/utils/multistoragebuffer.h>
#include <vkb/utils/TextureArrayDescriptorSet.h>
#include <vkb/vkb.h>

#include <gul/math/Transform.h>
#include <gul/MeshPrimitive.h>
#include <gul/ResourceLocator.h>

#include <gvk/Objects/Primtive.h>
#include <gvk/L1.h>

#include <glm/glm.hpp>

#include <imgui.h>
#include <imgui_impl_vulkan.h>
namespace gvk
{

struct ImguiRenderer
{
    struct InitInfo
    {
        VkInstance                      Instance;
        VkDevice                        Device = VK_NULL_HANDLE;
        VkPhysicalDevice                PhysicalDevice;
        uint32_t                        QueueFamily;
        VkQueue                         Queue;
        uint32_t                        MinImageCount=2;         // >= 2
        uint32_t                        ImageCount;             // >= MinImageCount

        VkRenderPass                    RenderPass;

        VkSampleCountFlagBits           MSAASamples;            // >= VK_SAMPLE_COUNT_1_BIT

        vkb::Storage * storage=nullptr;
        gvk::VulkanL2 * registry = nullptr;;
        gul::ResourceLocator * resourceLocator=nullptr;
    };

    InitInfo m_initInfo;
    VkDescriptorPool m_descriptorPool;

    void initResources(InitInfo I)
    {
        m_initInfo = I;

        vkb::DescriptorPoolCreateInfo2 ci;
        ci.maxSets = 10;
        ci.sizes.push_back(vk::DescriptorPoolSize(vk::DescriptorType::eUniformBuffer, 100));
        ci.sizes.push_back(vk::DescriptorPoolSize(vk::DescriptorType::eStorageBuffer, 100));
        ci.sizes.push_back(vk::DescriptorPoolSize(vk::DescriptorType::eCombinedImageSampler, 1000));

        m_descriptorPool = ci.create(*m_initInfo.storage, m_initInfo.Device);

        {
            // Setup Dear ImGui style
            ImGui_ImplVulkan_InitInfo init_info;
            uint32_t g_MinImageCount = 2;

            std::memset(&init_info,0, sizeof(init_info));
            init_info.Instance        = I.Instance;
            init_info.PhysicalDevice  = I.PhysicalDevice;
            init_info.Device          = I.Device;
            init_info.QueueFamily     = I.QueueFamily;
            init_info.Queue           = I.Queue;
            init_info.PipelineCache   = NULL;
            init_info.DescriptorPool  = m_descriptorPool;
            init_info.Allocator       = NULL;
            init_info.MinImageCount   = g_MinImageCount;
            init_info.ImageCount      = I.ImageCount;
            init_info.CheckVkResultFn = NULL;
            ImGui_ImplVulkan_Init(&init_info, I.RenderPass);
        }

        {
            m_initInfo.registry->beginCommandBuffer([](auto command_buffer)
            {
                ImGui_ImplVulkan_CreateFontsTexture(command_buffer);
            });
            ImGui_ImplVulkan_DestroyFontUploadObjects();
        }

    }

    void releaseResources()
    {
        ImGui_ImplVulkan_Shutdown();
        m_initInfo.storage->destroy(vk::DescriptorPool(m_descriptorPool), m_initInfo.Device);
    }

    void begin(VkCommandBuffer cmdBuff)
    {
        m_commandBuffer = cmdBuff;
        ImGui_ImplVulkan_NewFrame();
        ImGui::NewFrame();
    }
    void end()
    {
        ImGui::EndFrame();
        ImGui::Render();
        ImDrawData* draw_data = ImGui::GetDrawData();
        ImGui_ImplVulkan_RenderDrawData(draw_data, m_commandBuffer);
    }
    VkCommandBuffer m_commandBuffer;

    static void DrawVec3Control(const std::string& label, glm::vec3& values, float resetValue = 0.0f, float columnWidth = 100.0f)
    {
            ImGuiIO& io = ImGui::GetIO();
            auto boldFont = io.Fonts->Fonts[0];

            ImGui::PushID(label.c_str());

            ImGui::Columns(2);
            ImGui::SetColumnWidth(0, columnWidth);
            ImGui::Text(label.c_str());
            ImGui::NextColumn();

            //ImGui::PushMultiItemsWidths(3, ImGui::CalcItemWidth());
            ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2{ 0, 0 });

            float lineHeight =  4.0f;// GImGui->Font->FontSize + GImGui->Style.FramePadding.y * 2.0f;
            ImVec2 buttonSize = { lineHeight + 3.0f, lineHeight };

            ImGui::PushStyleColor(ImGuiCol_Button, ImVec4{ 0.8f, 0.1f, 0.15f, 1.0f });
            ImGui::PushStyleColor(ImGuiCol_ButtonHovered, ImVec4{ 0.9f, 0.2f, 0.2f, 1.0f });
            ImGui::PushStyleColor(ImGuiCol_ButtonActive, ImVec4{ 0.8f, 0.1f, 0.15f, 1.0f });
            ImGui::PushFont(boldFont);
            if (ImGui::Button("X", buttonSize))
                    values.x = resetValue;
            ImGui::PopFont();
            ImGui::PopStyleColor(3);

            ImGui::SameLine();
            ImGui::DragFloat("##X", &values.x, 0.1f, 0.0f, 0.0f, "%.2f");
            ImGui::PopItemWidth();
            ImGui::SameLine();

            ImGui::PushStyleColor(ImGuiCol_Button, ImVec4{ 0.2f, 0.7f, 0.2f, 1.0f });
            ImGui::PushStyleColor(ImGuiCol_ButtonHovered, ImVec4{ 0.3f, 0.8f, 0.3f, 1.0f });
            ImGui::PushStyleColor(ImGuiCol_ButtonActive, ImVec4{ 0.2f, 0.7f, 0.2f, 1.0f });
            ImGui::PushFont(boldFont);
            if (ImGui::Button("Y", buttonSize))
                    values.y = resetValue;
            ImGui::PopFont();
            ImGui::PopStyleColor(3);

            ImGui::SameLine();
            ImGui::DragFloat("##Y", &values.y, 0.1f, 0.0f, 0.0f, "%.2f");
            ImGui::PopItemWidth();
            ImGui::SameLine();

            ImGui::PushStyleColor(ImGuiCol_Button, ImVec4{ 0.1f, 0.25f, 0.8f, 1.0f });
            ImGui::PushStyleColor(ImGuiCol_ButtonHovered, ImVec4{ 0.2f, 0.35f, 0.9f, 1.0f });
            ImGui::PushStyleColor(ImGuiCol_ButtonActive, ImVec4{ 0.1f, 0.25f, 0.8f, 1.0f });
            ImGui::PushFont(boldFont);
            if (ImGui::Button("Z", buttonSize))
                    values.z = resetValue;
            ImGui::PopFont();
            ImGui::PopStyleColor(3);

            ImGui::SameLine();
            ImGui::DragFloat("##Z", &values.z, 0.1f, 0.0f, 0.0f, "%.2f");
            ImGui::PopItemWidth();

            ImGui::PopStyleVar();

            ImGui::Columns(1);

            ImGui::PopID();
    }
};

}
#endif
