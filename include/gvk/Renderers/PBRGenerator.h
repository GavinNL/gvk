#ifndef GVK_PBR_GENERATOR_H
#define GVK_PBR_GENERATOR_H

#include <vulkan/vulkan.h>
#include <vector>
#include <cstdint>
#include <exception>
#include <stdexcept>
#include <cassert>

#include <vkb/utils/DescriptorSetAllocator.h>
#include <vkb/vkb.h>

#include <gul/ResourceLocator.h>

#include <gvk/L1.h>

namespace gvk
{


/**
 * @brief The PBRTextureGenerator struct
 *
 * This class provides various compute capabilities
 * to produce textures and environment maps required by
 * the PBRPrimitiveRenderer.
 */
struct PBRTextureGenerator
{
    void initResources(VkDevice _device, gvk::VulkanL2 * _registry, gul::ResourceLocator * _resourceLocator);

    void releaseResources();

    /**
     * @brief convertToCube
     * @param dstCube
     * @param srcRect
     *
     * Convert a src rectangular image into a cube map. The src's dimension's width
     * must be 2x the height.
     *
     * Returns a unique pointer to a ScopedFence. If you discard this
     * it will block until the process is completed
     */
    std::unique_ptr<ScopedFence> convertToCube( gvk::ObjectID_t<Image> dstCube, gvk::ObjectID_t<Image> srcRect);

    /**
     * @brief generateSpMapInPlace
     * @param cube
     *
     * Generates the specular reflection map in place by taking
     * mipmap 0 and generating each of the radiance maps and copying
     * them into the subsequent mip levels.
     *
     *  The image should be in shader read only optimal form.
     */
    void generateSpMapInPlace( gvk::ObjectID_t<Image> cube);


    /**
     * @brief generateSpMap
     * @param dstCube
     * @param srcCube
     *
     * Generate the Radiance textures into dstCube. The mipmaps for dstCube must have
     * already been created.
     */
    void generateSpMap( gvk::ObjectID_t<Image> dstCube, gvk::ObjectID_t<Image> srcCube);

    /**
     * @brief generateIRMap
     * @param dstCube
     * @param srcCube
     *
     * Generate the Irradiance map from a source texture.
     */
    std::unique_ptr<ScopedFence> generateIRMap( gvk::ObjectID_t<Image> dstCube, gvk::ObjectID_t<Image> srcCube);


    /**
     * @brief generateBRDF
     * @param width
     * @return
     *
     * Generatre a BRDF image.
     */
    gvk::ObjectID_t<Image> generateBRDF(uint32_t width);

protected:


    // Generate a view for a particular cubeMap mip level
    VkImageView _generateMipMapView( gvk::ObjectID_t<Image> dstCube, uint32_t mipLevel);


    template<typename T>
    T const& get( gvk::ObjectID_t<T> i) const
    {
        return registry->get(i);
    }

    VkPipelineLayout getLayout(VkPipeline p) const;

    VkImageView getView(gvk::ObjectID_t<gvk::Image> id) const;

    VkDevice getDevice() const;

    VkDevice device = VK_NULL_HANDLE;
    gvk::VulkanL2 * registry = nullptr;
    gul::ResourceLocator * resourceLocator=nullptr;
    vkb::Storage m_storage;

    struct
    {
        struct
        {
            vk::Sampler  sampler;
            vk::Pipeline pipeline;
            vk::DescriptorSet set;
        } rectToCube;

        struct
        {
            //vk::Sampler  sampler;
            vk::Pipeline pipeline;
            vk::DescriptorSet set;
        } genSpMap;

        struct
        {
            //vk::Sampler  sampler;
            vk::Pipeline pipeline;
            vk::DescriptorSet set;
        } genIRMap;

        struct
        {
            //vk::Sampler  sampler;
            vk::Pipeline pipeline;
            vk::DescriptorSet set;
        } genBRDF;

        vkb::DescriptorSetAllocator dSetAllocator;

    } m_pipelines;

    void _initDSetAllocator();

    void _initComputePipelines();

    static vkb::PipelineLayoutCreateInfo2 _generatePipelineLayoutCreateInfo( std::vector< std::pair< std::vector<uint32_t>&, VkShaderStageFlagBits>> const & e);

};


}

#endif
