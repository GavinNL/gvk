#ifndef GVK_PRIMITIVE_RENDERER_H
#define GVK_PRIMITIVE_RENDERER_H

#include <vulkan/vulkan.h>
#include <vector>
#include <cstdint>
#include <exception>
#include <stdexcept>
#include <cassert>

#include <vkb/utils/DynamicPipeline.h>
#include <vkb/utils/DescriptorSetAllocator.h>
#include <vkb/utils/DescriptorUpdater.h>
#include <vkb/utils/multistoragebuffer.h>
#include <vkb/utils/TextureArrayDescriptorSet.h>
#include <vkb/utils/SPIRV_DescriptorSetLayoutGenerator.h>
#include <vkb/detail/ComputePipelineCreateInfo2.h>
#include <vkb/vkb.h>

#include <gul/math/Transform.h>
#include <gul/MeshPrimitive.h>
#include <gul/ResourceLocator.h>

#include <gvk/Objects/Primtive.h>
#include <gvk/Objects/PBRMaterial.h>
#include <gvk/L1.h>

#include <glm/glm.hpp>
#include <spdlog/spdlog.h>

#include "PBRGenerator.h"

namespace gvk
{

struct GlobalUniform
{

    glm::vec2   VIEWPORT_OFFSET;
    glm::vec2   VIEWPORT_DIM;

    float  TIMER_0 = 0.0f;
    float  TIMER_1 = 0.0f;
    float  TIMER_2 = 0.0f;
    float  TIMER_3 = 0.0f;

    float  FRAME_TIME_DELTA = 0.0f;
    int    FRAME_NUMBER     = 0;
    float  UNUSED0;
    float  UNUSED1;

    glm::vec2   MOUSE_POS;
    glm::vec2   UNUSED2;

    int    BRDF_INDEX   ;
    int    IRRMAP_INDEX ;
    int    RADMAP_INDEX ;
    uint   MOUSE_HOVER_UNIQUE_ID;

};

struct PushConstants
{
    uint32_t attributeFlags = 0;  // which vertex attributes are available
    uint32_t projMatrixIndex = 0;
    uint32_t viewMatrixIndex = 0;
    uint32_t projViewMatrixIndex = 0;

    uint32_t modelMatrixIndex = 0;
    uint32_t materialIndex0 = 0;
    uint32_t materialIndex1 = 0;
    uint32_t cameraMatrixIndex  = 0;

    uint32_t entityId;
    uint32_t unused0;
    uint32_t unused1;
    uint32_t unused2;
};

/**
 * @brief The KHRMaterial_shader struct
 *
 * This is the KHRMaterial data which is passed
 * into the shader as a StorageBuffer.
 */
struct KHRMaterial_shader
{
    glm::vec4 baseColorFactor    = {1,1,1,1};
    glm::vec4 emissiveFactor     = {0,0,0,0}; // only the RGB values are used
    float     metallicFactor     = 0.0f;
    float     roughnessFactor    = 0.5f;
    float     alphaCutoff        = 0.5f;
    float     vertexNormalOffset = 0.0f;

    int baseColorTextureIndex         = -1;
    int normalTextureIndex            = -1;
    int metallicRoughnessTextureIndex = -1;
    int occlusionTextureIndex         = -1;

    int emissiveTextureIndex          = -1;
    int unused0;
    int unused1;
    int unused2;
};
static_assert(sizeof(KHRMaterial_shader) == 80, "This material must be the same layout as the KHRMaterial defined in the KHR_PBR.frag/vert shaders");


/**
 * @brief The PBRPrimitiveRenderer struct
 *
 * This class is a higher level class for rendering PBR Primitives.
 *
 * Instead of binding buffers and textures, we bind Primitives and Materials.
 *
 *
 */
struct PBRPrimitiveRenderer
{
    struct BeginInfo
    {
        VkCommandBuffer commandBuffer;
        VkRenderPass    renderPass;
        glm::vec2       mousePos;
    };

    struct InitInfo
    {
        VkDevice device = VK_NULL_HANDLE;
        gvk::VulkanL2 * registry = nullptr;
        gul::ResourceLocator * resourceLocator=nullptr;
        PBRTextureGenerator * textureGenerator=nullptr;
    };
    uint32_t m_IrradianceMapSize = 32;
    uint32_t m_RadianceMapSize   = 1024;

    static constexpr uint32_t MAX_TRANSFORMS     = 10000;
    static constexpr uint32_t MAX_MATERIALS      = 10000;
    static constexpr uint32_t MAX_DEPTH_BUCKETS  = 4096;

    gvk::ObjectID_t<gvk::Buffer> m_shaderWriteBuffer;
    gvk::ObjectID_t<gvk::Buffer> m_transformBuffer;
    vkb::MultiStorageBuffer      m_transformStorage;

    gvk::ObjectID_t<gvk::Buffer> m_PBRMaterialBuffer;
    vkb::MultiStorageBuffer      m_materialStorage;

    gvk::ObjectID_t<gvk::Buffer> m_GlobalUniform;
    GlobalUniform                m_GlobalUniformData;

    VkDescriptorSet  m_storageBufferDescriptorSet;

    vkb::Storage m_storage;
    vkb::TextureArrayDescriptorSetChain m_imageDescriptorChain;

    gvk::ObjectID_t<gvk::Image> m_BRDF;
    gvk::ObjectID_t<gvk::Image> m_EnvMap; // Radiance texture map
    gvk::ObjectID_t<gvk::Image> m_IrrMap; // Irradiance texture map

    uint32_t m_uniqueIDMouseHover=0;

    void initResources(InitInfo I);


    void releaseResources();


    /**
     * @brief generateNewEnvironment
     * @param rectImage
     *
     * Given a rectangular panoramic image, update the current
     * SP and IRR maps using the data.
     *
     * This function does not allocate any new data. All images
     * are copied directly from the rectImage to the previously
     * allocated images.
     *
     * This is a blocking call, it will block until the
     * entire environment is generated.
     */
    void generateNewEnvironment(gvk::ObjectID_t<Image> rectImage);

    /**
     * @brief stageTexture
     * @param img
     *
     * Stage a texture for use on the next draw cycle.
     * You must stage all textures you want to use
     * before you call begin().
     *
     * you may stage a texture while you are writing the
     * command buffer ,but the image will not be available
     * until the next call to begin()
     */
    int32_t stageTexture( gvk::ObjectID_t<Image> img, VkSampler sampler=VK_NULL_HANDLE);

    /**
     * @brief unstageTexture
     * @param img
     * @return
     *
     * Unstage a texture from the renderer.
     */
    int32_t unstageTexture( gvk::ObjectID_t<Image> img);


    /**
     * @brief beginRender
     * @param B
     *
     * This should be called before you attempt to render any objects.
     * When this is called, any textures which have been staged that haven't been
     * added to the descriptor set will be added.
     */
    void beginRender(BeginInfo B);

    /**
     * @brief endRender
     *
     * End the render.
     */
    void endRender();

    /**
     * @brief getMouseHoverID
     * @return
     *
     * Returns the unique ID of the object that the mouse
     * is currently hovering over. This value is only
     * valud if after endRender() is called and after
     * the command buffers have executed.
     *
     * This value may be delayed by 1 or 2 frames
     */
    uint32_t getMouseHoverID() const;



    /**
     * @brief bindPrimitive
     * @param cmd
     * @param pID
     *
     * bind a primitive to the command buffer. This binds all
     * the vertex attributes at their specific binding point
     *
     */
    void bindPrimitive(gvk::ObjectID_t<Primitive> pID);

    /**
     * @brief draw
     * @param instanceCount
     * @param firstInstance
     *
     * Draw the last bound primitive with the last matrix that
     * was set using setModelMatrix.
     *
     * If instanceCount is greater than one, you should
     * use setModelMatrices() first to push N matrices
     * into the buffer, and then use draw with instanceCount=N
     *
     */
    void draw(uint32_t instanceCount=1, uint32_t firstInstance=0);


    /**
     * @brief setProjMatrix
     * @param P
     *
     * Set the projection matrix used for rendering
     */
    void setProjMatrix(glm::mat4 const &P);

    /**
     * @brief setViewMatrix
     * @param V
     *
     * Set the view matrix used for rendering
     */
    void setViewMatrix(glm::mat4 const &V);

    /**
     * @brief setProjViewMatrix
     * @param PV
     *
     * Set the project*vew matrix used for rendering.
     * This does NOT set the view and projection matrix.
     * This is a separate matrix slot used for storing
     * the combined Proj*View matrix.
     */
    void setProjViewMatrix(glm::mat4 const &PV);

    /**
     * @brief setModelMatrix
     * @param M
     *
     * Sets the model matrix used for rendering primitives.
     */
    int32_t setModelMatrix(glm::mat4 const &M);

    /**
     * @brief setModelMatrices
     * @param M
     * @param i
     *
     * send multiple model matrices to the buffer. This
     * is used in conjunction with draw instances.
     * This can push N matrices, but returns the
     * index to the first one.
     */
    int32_t setModelMatrices(glm::mat4 const * M, size_t N);


    /**
     * @brief generateMaterialUniform
     * @param mat
     * @return
     *
     * Given a material ID, generate the data
     * which must be passed into pushMaterial.
     *
     * This may be removed.
     */
    KHRMaterial_shader generateMaterialUniform(gvk::ObjectID_t<PBRMaterial> mat);

    /**
     * @brief pushMaterial
     * @param Ms
     * @param materialNumber
     *
     * Push a material to the
     */
    void setMaterial( KHRMaterial_shader const &Ms, uint32_t materialNumber=0);
    /**
     * @brief setMaterial
     * @param mat
     * @param materialNumber
     *
     * Sets the current material. Any primitives drawn will then use
     * this material.
     */
    void setMaterial(gvk::ObjectID_t<PBRMaterial> mat, uint32_t materialNumber=0);


    /**
     * @brief resetTimer
     * @param timerIndex
     * @param value
     *
     * There are 4 timers which are accessable in the shader's global uniform
     */
    void resetTimer(uint32_t timerIndex = 0, float value = 0.0f );
    /**
     * @brief setEntityID
     * @param id
     *
     * Sets the entity ID. This id is used inside the shader
     * to render to the ID Buffer. It is used for mouse picking.
     */
    void setEntityID(uint32_t id);

    void setViewPort( VkViewport vp);

    void setScissor(  VkRect2D vp);


protected:
    template<typename T>
    T const& get( gvk::ObjectID_t<T> i) const
    {
        return m_initInfo.registry->get(i);
    }

    VkPipelineLayout getLayout(VkPipeline p) const;
    VkImageView getView(gvk::ObjectID_t<gvk::Image> id) const;


    InitInfo  m_initInfo;
    BeginInfo m_info;

    gvk::ObjectID_t<Primitive>   m_lastPrimitive;

    uint32_t m_vertexCount = 0;
    uint32_t m_indexCount  = 0;


    vkb::DynamicPipeline             m_KHR_Pipeline;
    vkb::DynamicPipeline::value_type m_piplineBind;

    std::chrono::system_clock::time_point m_timePoint = std::chrono::system_clock::now();

    struct
    {
        vkb::DescriptorSetAllocator dSetAllocator;

    } m_pipelines;

    void initDSetAllocator();

    void initPipeline();

    static vkb::PipelineLayoutCreateInfo2 _generatePipelineLayoutCreateInfo( std::vector< std::pair< std::vector<uint32_t>&, VkShaderStageFlagBits>> const & shaderCode);

    template<typename T>
    bool valid( gvk::ObjectID_t<T> const & B) const
    {
        return m_initInfo.registry->m_registry.valid(B.m_entity);
    }

};


}

#endif
