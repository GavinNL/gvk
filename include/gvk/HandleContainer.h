#ifndef GVK_HANDLE_CONTAINER_H
#define GVK_HANDLE_CONTAINER_H

#include <vulkan/vulkan.h>
#include <vector>
#include <cstdint>
#include <exception>
#include <stdexcept>
#include <cassert>
#include <entt/entt.hpp>

#include "ObjectID.h"

namespace gvk
{

/**
 * @brief The HandleContainer struct
 *
 * The HandleContainer is a wrapper around an entt::registry,
 * but instead of being used as an entity-component-system,
 * it is used to store objects handles.
 *
 * It acts similar to a smart pointer, but instead of giving
 * you an actual pointer, it gives you an entity which
 * can be looked up..
 *
 * HandleContainer H;
 * auto id = H.objectCreate<MyData>();
 *
 * MyData & D = H.get(id);
 *
 */
struct HandleContainer
{
    using entity_type   = HandleID;
    using registry_type = entt::basic_registry<entity_type>;


    /**
     * @brief make_handle
     * @param __args
     * @return
     *
     * This is the main method of creating a handle,
     * It works just like std::make_shared or std::make_unique
     */
    template<typename _Tp, typename... _Args>
    inline ObjectID_t<_Tp> make_handle(_Args&&... __args)
    {
      typedef typename std::remove_cv<_Tp>::type _Tp_nc;

      ObjectID_t<_Tp_nc> id;
      id.m_entity = m_registry.create();
      m_registry.emplace<_Tp_nc>(id.m_entity, std::forward<_Args>(__args)...);
      return id;
    }

    template<typename T>
    T const & get(ObjectID_t<T> id) const
    {
        return m_registry.get<T>(id.m_entity);
    }

    template<typename T>
    T& get(ObjectID_t<T> id)
    {
        return m_registry.get<T>(id.m_entity);
    }

    registry_type m_registry;
};
}

#endif
