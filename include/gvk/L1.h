#ifndef GVK_L1_H
#define GVK_L1_H

#include <vector>
#include <cstdint>
#include <exception>
#include <stdexcept>
#include <cassert>
#include <cmath>
#include <iostream>

// conan packages
#include <vulkan/vulkan.hpp>
#include <vk_mem_alloc.h>

// third_party packages
#include <GLSLCompiler.h>

#include "HandleContainer.h"
#include "VulkanStatic.h"
#include "logging.h"

#include <vkb/vkb.h>

#include "Objects/Buffer.h"
#include "Objects/Image.h"
#include "Objects/Primtive.h"

#include <gul/MeshPrimitive.h>
#include <gul/Image.h>

namespace gvk
{
#if defined(__ANDROID__)
#define VK_CHECK_RESULT(f)																				\
{																										\
    VkResult res = (f);																					\
    if (res != VK_SUCCESS)																				\
    {																									\
        LOGE("Fatal : VkResult is \" %d \" in %s at line %d", res, __FILE__, __LINE__);					\
        assert(res == VK_SUCCESS);																		\
    }																									\
}
#else
#define VK_CHECK_RESULT(f)																				\
{																										\
    VkResult res = (f);																					\
    if (res != VK_SUCCESS)																				\
    {																									\
        std::cout << "Fatal : VkResult is \"" << res << "\" in " << __FILE__ << " at line " << __LINE__ << std::endl; \
        assert(res == VK_SUCCESS);																		\
    }																									\
}
#endif

struct ScopedFence
{
    ~ScopedFence()
    {
        if( m_fence != VK_NULL_HANDLE)
        {
            wait();
            destroy();
        }
    }

    ScopedFence()
    {
    }
    ScopedFence(ScopedFence const &) = delete ;
    ScopedFence& operator = (ScopedFence const&) = delete ;

    ScopedFence(ScopedFence && A) = default;
    ScopedFence& operator = (ScopedFence && A) = default;

    void wait() const
    {
        spdlog::info("Waiting on fence");
        VK_CHECK_RESULT(vkWaitForFences(m_device, 1, &m_fence, VK_TRUE, 100000000000));
        spdlog::info("Waiting finished");
    }

    bool ready() const
    {
        return VK_SUCCESS == vkGetFenceStatus(m_device, m_fence);
    }

    void destroy()
    {
        if( m_fence != VK_NULL_HANDLE)
        {
            vkDestroyFence(m_device, m_fence, nullptr);
        }
        if( m_buffer != VK_NULL_HANDLE)
        {
            vkFreeCommandBuffers(m_device, m_pool, 1, &m_buffer);
        }

        m_device = VK_NULL_HANDLE;
        m_fence = VK_NULL_HANDLE;
        m_pool= VK_NULL_HANDLE;
        m_buffer= VK_NULL_HANDLE;
    }

    VkDevice m_device = VK_NULL_HANDLE;
    VkFence m_fence = VK_NULL_HANDLE;
    VkCommandPool m_pool= VK_NULL_HANDLE;
    VkCommandBuffer m_buffer= VK_NULL_HANDLE;
};

/**
 * @brief The VulkanL1 struct
 *
 * The Vulkan_L1 (level 1) struct handles
 * all the lowest level of vulkan handling:
 *  Creating buffers
 *  Creating images
 *  commandpools
 *  mapping memory
 *
 */
struct VulkanL1 : public HandleContainer,
                  public VulkanStatic
{
    //---------------- these values need to be passed in
    VkDevice              m_device;
    VkPhysicalDevice      m_physicalDevice;
    VkInstance            m_instance;

    VkQueue               m_graphicsQueue;

    // ---------------- these are created internally
    VmaAllocator            m_allocator = nullptr;

    //------------------
    VkCommandPool m_TransferCommandPool; // used for transfer buffers


    vkb::Storage m_storage;

    /**
     * @brief init
     * @param instance
     * @param physicalDevice
     * @param device
     * @param graphicsQueue
     * @return
     *
     * Initialize the vulkan data by providing
     * the instance, physical device logical device
     * and graphics queue.
     *
     *
     */
    bool initVulkan(vk::Instance instance, vk::PhysicalDevice physicalDevice, vk::Device device, vk::Queue graphicsQueue)
    {
        if( m_allocator == nullptr)
        {
            VmaAllocatorCreateInfo allocatorInfo = {};

            allocatorInfo.physicalDevice = physicalDevice;
            allocatorInfo.device         = device;
            allocatorInfo.instance       = instance;
            auto result = vmaCreateAllocator(&allocatorInfo, &m_allocator);

            if( result != VK_SUCCESS)
                vk::throwResultException( vk::Result(result), "Error creating allocator");

            m_instance = instance;
            m_device = device;
            m_physicalDevice = physicalDevice;
            m_graphicsQueue = graphicsQueue;

            m_TransferCommandPool = commandPool_Create(VK_QUEUE_TRANSFER_BIT | VK_QUEUE_GRAPHICS_BIT);

            return true;
        }
        return false;
    }

    VkSampler getSampler( VkFilter filter, VkSamplerAddressMode addressMode)
    {
        // Temporary 1-mipmap sampler
        vkb::SamplerCreateInfo2 ci;
        ci.magFilter               =  vk::Filter(filter);
        ci.minFilter               =  vk::Filter(filter);
        ci.mipmapMode              =  vk::SamplerMipmapMode::eLinear ;
        ci.addressModeU            =  vk::SamplerAddressMode(addressMode);
        ci.addressModeV            =  vk::SamplerAddressMode(addressMode);
        ci.addressModeW            =  vk::SamplerAddressMode(addressMode);
        ci.mipLodBias              =  0.0f  ;
        ci.anisotropyEnable        =  VK_FALSE;
        ci.maxAnisotropy           =  1 ;
        ci.compareEnable           =  VK_FALSE ;
        ci.compareOp               =  vk::CompareOp::eAlways;
        ci.minLod                  =  0 ;
        ci.maxLod                  =  VK_LOD_CLAMP_NONE;
        ci.borderColor             =  vk::BorderColor::eIntOpaqueBlack ;
        ci.unnormalizedCoordinates =  VK_FALSE ;

        return ci.create( m_storage, m_device);
    }

    /**
     * @brief shutdown
     *
     * Shutdown the class by destroying all objects
     */
    void shutdown()
    {
        // destroy all the buffers
        for(auto e : m_registry.view<Buffer>() )
        {
            ObjectID_t<Buffer> b;
            b.m_entity = e;

            buffer_Destroy(b);
        }
        // destroy all the images
        for(auto e : m_registry.view<Image>() )
        {
            ObjectID_t<Image> b;
            b.m_entity = e;

            image_Destroy(b);
        }

        m_storage.destroyAll(m_device);

        vkDestroyCommandPool(m_device, m_TransferCommandPool, nullptr);

        vmaDestroyAllocator(m_allocator);
        m_allocator = nullptr;
    }

    /**
     * @brief createBuffer
     * @param size
     * @param usage
     * @param memUsage
     *
     * @return
     *
     * Create a buffer and return the object id.
     */
    ObjectID_t<Buffer> buffer_Create(VkDeviceSize size, VkBufferUsageFlags usage, VmaMemoryUsage memUsage)
    {
        VkBufferCreateInfo bufferInfo = {};
        bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
        bufferInfo.size  = size;
        bufferInfo.usage = usage;

        VmaAllocationCreateInfo allocCInfo = {};
        allocCInfo.usage = memUsage;
        allocCInfo.flags = VMA_ALLOCATION_CREATE_MAPPED_BIT; // can always set this bit,
                                                             // vma will not allow device local
                                                             // memory to be mapped

        VkBuffer          buffer;
        VmaAllocation     allocation = nullptr;
        VmaAllocationInfo allocInfo;

        auto result = vmaCreateBuffer(m_allocator, &bufferInfo, &allocCInfo, &buffer, &allocation, &allocInfo);

        if( result != VK_SUCCESS)
            vk::throwResultException( vk::Result(result), "Error allocating VMA Buffer");

        auto id = make_handle<Buffer>();
        auto & B= get(id);

        B._buffer        = buffer;
        B._allocation    = allocation;
        B._allocInfo     = allocInfo;
        B.createInfo     = bufferInfo;
        B._requestedSize = size;
        return id;
    }

    /**
     * @brief destroyBuffer
     * @param b
     *
     * Destroy a buffer
     */
    void buffer_Destroy(ObjectID_t<Buffer> b)
    {
        auto & B = get(b);

        vmaDestroyBuffer(m_allocator, B._buffer, B._allocation);

        LDEBUG("Buffer Destroyed. {} bytes freed", B.createInfo.size);
        B._allocInfo = {};
        B._allocation = nullptr;
        B._buffer = vk::Buffer();

        m_registry.destroy(b.m_entity);
    }

    //-------------------------------
    //
    // BUFFER functions
    //
    //-------------------------------


    void* bufferMapData(ObjectID_t<Buffer> b)
    {
        auto & S = get(b);
        void *mapped=nullptr;

        VK_CHECK_RESULT( vmaMapMemory( m_allocator, S._allocation, &mapped ) );

        return mapped;
    }

    void bufferUnmapData(ObjectID_t<Buffer> b)
    {
        auto & S = get(b);
        vmaUnmapMemory(m_allocator, S._allocation);
    }

    void bufferFlushData(ObjectID_t<Buffer> b, uint32_t bufferOffset=0, vk::DeviceSize byteSize=VK_WHOLE_SIZE)
    {
        auto & S = get(b);
        vmaFlushAllocation(m_allocator, S._allocation, bufferOffset, byteSize);
    }

    void bufferCopyData( ObjectID_t<Buffer> b, void const * data, uint32_t byteSize, uint32_t bufferOffset)
    {
        auto stagingBuffer = buffer_Create(byteSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VMA_MEMORY_USAGE_CPU_TO_GPU);

        auto mapped = bufferMapData(stagingBuffer);
        std::memcpy(mapped, data, byteSize);

        bufferFlushData(stagingBuffer,0, byteSize);
        bufferUnmapData(stagingBuffer);

        {
            auto & S = get(stagingBuffer);
            auto & D = get(b);

            beginCommandBuffer([&](VkCommandBuffer cmd)
            {
                VkBufferCopy copyRegion = {};

                copyRegion.dstOffset = bufferOffset;
                copyRegion.srcOffset = 0;
                copyRegion.size = byteSize;

                vkCmdCopyBuffer(cmd, S._buffer, D._buffer, 1, &copyRegion);
            });
        }

        buffer_Destroy(stagingBuffer);

    }

    //-------------------------------
    // IMAGE functions

    ObjectID_t<Image> image2D_Create(uint32_t width, uint32_t height, VkFormat format = VK_FORMAT_R8G8B8A8_UNORM, uint32_t arrayLayers=1, uint32_t mipLevels=0, VkImageUsageFlags additionalUsageFlags = {})
    {
        return image_Create(width, height, 1, format, arrayLayers > 1 ? VK_IMAGE_VIEW_TYPE_2D_ARRAY : VK_IMAGE_VIEW_TYPE_2D , arrayLayers, mipLevels, additionalUsageFlags);
    }

    ObjectID_t<Image> imageCube_Create(uint32_t sideLength, VkFormat format, uint32_t mipLevels=0, VkImageUsageFlags additionalUsageFlags={})
    {
        return image_Create(sideLength, sideLength, 1, format, VK_IMAGE_VIEW_TYPE_CUBE , 6, mipLevels, additionalUsageFlags);
    }

    ObjectID_t<Image>  image_Create( uint32_t width, uint32_t height, uint32_t depth
                                    ,VkFormat format
                                    ,VkImageViewType viewType
                                    ,uint32_t arrayLayers
                                    ,uint32_t miplevels // maximum mip levels
                                    ,VkImageUsageFlags additionalUsageFlags
                                    )
    {
        VkImageCreateInfo imageInfo{};

        imageInfo.sType         = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
        imageInfo.imageType     = depth==1 ? VK_IMAGE_TYPE_2D : VK_IMAGE_TYPE_3D;
        imageInfo.format        = format;
        imageInfo.extent        = vk::Extent3D(width, height, depth);
        imageInfo.mipLevels     = miplevels;
        imageInfo.arrayLayers   = arrayLayers;

        if( imageInfo.mipLevels == 0)
        {
            auto w = width;
            auto h = std::max(w,height);
            auto d = std::max(h,depth);
            imageInfo.mipLevels = 1 + static_cast<uint32_t>(std::floor(std::log2(d)));
            miplevels = imageInfo.mipLevels;
        }

        imageInfo.samples       = VK_SAMPLE_COUNT_1_BIT;// vk::SampleCountFlagBits::e1;
        imageInfo.tiling        = VK_IMAGE_TILING_OPTIMAL;// vk::ImageTiling::eOptimal;
        imageInfo.usage         = additionalUsageFlags | VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT;// vk::ImageUsageFlagBits::eSampled | vk::ImageUsageFlagBits::eTransferSrc | vk::ImageUsageFlagBits::eTransferDst;
        imageInfo.sharingMode   = VK_SHARING_MODE_EXCLUSIVE;// vk::SharingMode::eExclusive;
        imageInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;// vk::ImageLayout::eUndefined;

        if( arrayLayers == 6)
            imageInfo.flags |=  VK_IMAGE_CREATE_CUBE_COMPATIBLE_BIT;// vk::ImageCreateFlagBits::eCubeCompatible;

        VmaAllocationCreateInfo allocCInfo = {};
        allocCInfo.usage = VMA_MEMORY_USAGE_GPU_ONLY;

        VkImage image;
        VmaAllocation allocation;
        VmaAllocationInfo allocInfo;

        VkImageCreateInfo & imageInfo_c = imageInfo;
        vmaCreateImage(m_allocator,  &imageInfo_c,  &allocCInfo, &image, &allocation, &allocInfo);

        auto id = make_handle<Image>();
        auto & I = get(id);

        I.imageCreateInfo = imageInfo;

        I._image = image;
        I._allocInfo = allocInfo;
        I._viewType = viewType;

        // create the image view
        {
            {
                VkImageViewCreateInfo ci{};
                ci.sType    = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
                ci.image    = I._image;
                ci.viewType = viewType;
                ci.format   = format;
                ci.components = { VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_G, VK_COMPONENT_SWIZZLE_B, VK_COMPONENT_SWIZZLE_A };

                ci.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;

                ci.subresourceRange.baseMipLevel = 0;
                ci.subresourceRange.levelCount = VK_REMAINING_MIP_LEVELS;

                ci.subresourceRange.baseArrayLayer = 0;
                ci.subresourceRange.layerCount = VK_REMAINING_ARRAY_LAYERS;

                switch(ci.format)
                {
                    case VK_FORMAT_D16_UNORM:
                    case VK_FORMAT_D32_SFLOAT:
                    case VK_FORMAT_D16_UNORM_S8_UINT:
                    case VK_FORMAT_D24_UNORM_S8_UINT:
                    case VK_FORMAT_D32_SFLOAT_S8_UINT:
                        ci.subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;// vk::ImageAspectFlagBits::eDepth;
                        break;
                    default:
                        ci.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT; //vk::ImageAspectFlagBits::eColor;
                        break;
                }

                VK_CHECK_RESULT( vkCreateImageView(m_device, &ci, nullptr, &I._view) );
            }

        }


        // create a sampler
        {
            // Temporary 1-mipmap sampler
            vkb::SamplerCreateInfo2 ci;
            ci.magFilter               =  vk::Filter::eLinear;
            ci.minFilter               =  vk::Filter::eLinear;
            ci.mipmapMode              =  vk::SamplerMipmapMode::eLinear ;
            ci.addressModeU            =  vk::SamplerAddressMode::eRepeat ;
            ci.addressModeV            =  vk::SamplerAddressMode::eRepeat ;
            ci.addressModeW            =  vk::SamplerAddressMode::eRepeat ;
            ci.mipLodBias              =  0.0f  ;
            ci.anisotropyEnable        =  VK_FALSE;
            ci.maxAnisotropy           =  1 ;
            ci.compareEnable           =  VK_FALSE ;
            ci.compareOp               =  vk::CompareOp::eAlways ;
            ci.minLod                  =  0 ;
            ci.maxLod                  =  static_cast<float>(miplevels) ;
            ci.borderColor             =  vk::BorderColor::eIntOpaqueBlack ;
            ci.unnormalizedCoordinates =  VK_FALSE ;

            ci.magFilter               =  vk::Filter::eLinear;
            ci.minFilter               =  vk::Filter::eLinear;

            I.sampler.linear = ci.create( m_storage, m_device);


            ci.magFilter               =  vk::Filter::eNearest;
            ci.minFilter               =  vk::Filter::eNearest;

            I.sampler.nearest = ci.create( m_storage, m_device);
        }

        return id;
    }


    void image_Destroy(ObjectID_t<Image> id)
    {
        {
            auto & I = get(id);

            LDEBUG("Image Destroyed: {}x{}x{}",  I.imageCreateInfo.extent.width, I.imageCreateInfo.extent.height, I.imageCreateInfo.extent.depth);

            vk::Device(m_device).destroyImageView(I._view);

            vmaDestroyImage(m_allocator, I._image, I._allocation);
            I._allocation = nullptr;
            I._image = vk::Image();
            I._view = vk::ImageView();
        }
        m_registry.destroy(id.m_entity);
    }


    //-------------------------------
    //
    // Command Buffer functions
    //


    /**
     * @brief beginCommandBuffer
     * @param c
     *
     * Allocate a command buffer and use a callable to
     * write the buffer.
     * The commandbuffer will automatically be submitted
     * at the end of the call and then freed.
     *
     * beginCommandBuffer([](VkCommandBuffer cmd)
     * {
     *
     * });
     */
    template<typename Callable_t>
    std::unique_ptr<ScopedFence> beginCommandBuffer(Callable_t && c)
    {
        VkCommandBuffer cmd = commandBuffer_Create(VK_COMMAND_BUFFER_LEVEL_PRIMARY, true);

        c(cmd);

        VK_CHECK_RESULT(vkEndCommandBuffer(cmd));

        #if 1
            {
                VkSubmitInfo submitInfo{};
                submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
                submitInfo.commandBufferCount = 1;
                submitInfo.pCommandBuffers = &cmd;

                {
                    // Create fence to ensure that the command buffer has finished executing
                    VkFenceCreateInfo fenceInfo{};
                    fenceInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
                    VkFence fence;
                    VK_CHECK_RESULT(vkCreateFence(m_device, &fenceInfo, nullptr, &fence));

                    VK_CHECK_RESULT(vkQueueSubmit(m_graphicsQueue, 1, &submitInfo, fence));

                    auto D = std::make_unique<ScopedFence>();
                    D->m_fence  = fence;
                    D->m_device = m_device;
                    D->m_pool   = m_TransferCommandPool;
                    D->m_buffer = cmd;

                    return D;
                }
            }
        #else
        commandBuffer_Submit(cmd, m_graphicsQueue,true);
        commandBuffer_Free(cmd);
        #endif
    }


    VkCommandBuffer commandBuffer_Create(VkCommandBufferLevel level, bool begin)
    {
        VkCommandBufferAllocateInfo cmdBufAllocateInfo{};
        cmdBufAllocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
        cmdBufAllocateInfo.commandPool = m_TransferCommandPool;
        cmdBufAllocateInfo.level = level;
        cmdBufAllocateInfo.commandBufferCount = 1;

        VkCommandBuffer cmdBuffer;
        VK_CHECK_RESULT(vkAllocateCommandBuffers(m_device, &cmdBufAllocateInfo, &cmdBuffer));

        // If requested, also start recording for the new command buffer
        if (begin)
        {
            VkCommandBufferBeginInfo commandBufferBI{};
            commandBufferBI.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
            VK_CHECK_RESULT(vkBeginCommandBuffer(cmdBuffer, &commandBufferBI));
        }

        return cmdBuffer;
    }

    void commandBuffer_Free(VkCommandBuffer cmd)
    {
        vkFreeCommandBuffers(m_device, m_TransferCommandPool, 1, &cmd);
    }

    void commandBuffer_Submit(VkCommandBuffer commandBuffer, VkQueue queue, bool waitTillFinished)
    {
        VkSubmitInfo submitInfo{};
        submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
        submitInfo.commandBufferCount = 1;
        submitInfo.pCommandBuffers = &commandBuffer;

        if( waitTillFinished )
        {
            // Create fence to ensure that the command buffer has finished executing
            VkFenceCreateInfo fenceInfo{};
            fenceInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
            VkFence fence;
            VK_CHECK_RESULT(vkCreateFence(m_device, &fenceInfo, nullptr, &fence));

            VK_CHECK_RESULT(vkQueueSubmit(queue, 1, &submitInfo, fence));

            VK_CHECK_RESULT(vkWaitForFences(m_device, 1, &fence, VK_TRUE, 100000000000));

            vkDestroyFence(m_device, fence, nullptr);
        }
        else
        {
            VK_CHECK_RESULT(vkQueueSubmit(queue, 1, &submitInfo, nullptr));
        }
    }

    VkCommandPool commandPool_Create( VkQueueFlags queueFlagBits, VkCommandPoolCreateFlags createFlags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT)
    {
        VkCommandPoolCreateInfo cmdPoolInfo = {};
        cmdPoolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;

        cmdPoolInfo.queueFamilyIndex = getQueueFamilyIndex(queueFlagBits, m_physicalDevice);

        cmdPoolInfo.flags = createFlags;
        VkCommandPool cmdPool;
        VK_CHECK_RESULT(vkCreateCommandPool(m_device, &cmdPoolInfo, nullptr, &cmdPool));
        return cmdPool;
    }
};


/**
 * @brief The VulkanL2 struct
 *
 * VulkanL2 adds various command buff functions
 */
struct VulkanL2 : public VulkanL1
{
    /**
     * @brief image_copyFromBuffer2
     * @param cmd
     *
     * @param srcBuffer          - the source buffer
     * @param srcBufferOffset    - the offset from the buffer's start where pixel 0,0,0 would be located
     * @param srcBufferImageSize - the dimensions of the image that is stored in the buffer.
     * @param srcBytesPerPixel   - the bytes per pixel of the image in the buffer
     * @param srcImageCopyOffset - the texel offset that indicates the start of the subregion to copy from
     *
     * @param dstImage           - the destination image we are going to copy into
     * @param dstArrayLayer      - the array layer to copy intoo
     * @param dstMipLevel        - the mip level to copy into
     * @param dstImagePasteOffset- the destinaion offset where we want to paste into
     *
     * @param dimensionsToCopy   - the width/height of the area we want to copy
     *
     * Copy a region of texels from a buffer into a VkImage.
     *
     */
    void image_copyFromBuffer2( VkCommandBuffer    cmd,

                                ObjectID_t<Buffer> srcBuffer,            // which buffer currently stores the image
                                VkDeviceSize       srcBufferOffset,      // an offset into the buffer where the first pixel of the image is
                                VkExtent2D         srcBufferImageSize,   // the actual size of the FULL buffer image
                                uint32_t           srcBytesPerPixel,
                                VkOffset2D         srcImageCopyOffset,   // the starting offset of where to copy from

                                ObjectID_t<Image>  dstImage,        // which image we are copying into
                                uint32_t           dstArrayLayer,   // which layer we are copying into
                                uint32_t           dstMipLevel,      // which mip level we're copying into
                                VkOffset2D         dstImagePasteOffset,    // the destination offset where we are going to paste

                                VkExtent2D         dimensionsToCopy  // number of pixels to copy
                               )
    {
        std::vector<VkBufferImageCopy> regions;
        regions.emplace_back() =
           generateBufferImageCopy(srcBufferOffset, srcBufferImageSize, srcBytesPerPixel, srcImageCopyOffset,dstArrayLayer, dstMipLevel,dstImagePasteOffset, dimensionsToCopy);
        image_copyFromBuffer(cmd, dstImage, srcBuffer, regions);
    }


    /**
     * @brief generateBufferImageCopy
     *
     * @param srcBufferOffset    - the offset from the buffer's start where pixel 0,0,0 would be located
     * @param srcBufferImageSize - the dimensions of the image that is stored in the buffer.
     * @param srcBytesPerPixel   - the bytes per pixel of the image in the buffer
     * @param srcImageCopyOffset - the texel offset that indicates the start of the subregion to copy from
     *
     * @param dstArrayLayer      - the array layer to copy intoo
     * @param dstMipLevel        - the mip level to copy into
     * @param dstImagePasteOffset- the destinaion offset where we want to paste into
     *
     * @param dimensionsToCopy   - the width/height of the area we want to copy
     *
     * Copy a region of texels from a buffer into a VkImage.
     */
    static VkBufferImageCopy generateBufferImageCopy(
                                VkDeviceSize       srcBufferOffset,      // an offset into the buffer where the first pixel of the image is
                                VkExtent2D         srcBufferImageSize,   // the actual size of the FULL buffer image
                                uint32_t           srcBytesPerPixel,
                                VkOffset2D         srcImageCopyOffset,   // the starting offset of where to copy from

                                uint32_t           dstArrayLayer,   // which layer we are copying into
                                uint32_t           dstMipLevel,      // which mip level we're copying into
                                VkOffset2D         dstImagePasteOffset,    // the destination offset where we are going to paste

                                VkExtent2D         dimensionsToCopy  // number of pixels to copy
                               )
    {
        std::vector<VkBufferImageCopy> regions;

        VkBufferImageCopy bufferCopyRegion;
        bufferCopyRegion.imageSubresource.aspectMask     = VK_IMAGE_ASPECT_COLOR_BIT;
        bufferCopyRegion.imageSubresource.mipLevel       = dstMipLevel;
        bufferCopyRegion.imageSubresource.baseArrayLayer = dstArrayLayer;
        bufferCopyRegion.imageSubresource.layerCount     = 1;

        bufferCopyRegion.imageOffset.x        = dstImagePasteOffset.x;
        bufferCopyRegion.imageOffset.y        = dstImagePasteOffset.y;
        bufferCopyRegion.imageOffset.z        = 0;

        bufferCopyRegion.imageExtent.width  = dimensionsToCopy.width;
        bufferCopyRegion.imageExtent.height = dimensionsToCopy.height;
        bufferCopyRegion.imageExtent.depth  = 1;


        bufferCopyRegion.bufferRowLength    = srcBufferImageSize.width;
        bufferCopyRegion.bufferImageHeight  = srcBufferImageSize.height;

        uint32_t bytesPerPixel = srcBytesPerPixel;//
        uint32_t bytesPerRow   = bytesPerPixel * bufferCopyRegion.bufferRowLength;

        bufferCopyRegion.bufferOffset       = srcBufferOffset +
                                                    static_cast<uint32_t>(srcImageCopyOffset.y) * bytesPerRow + static_cast<uint32_t>(srcImageCopyOffset.x) * bytesPerPixel;

        return bufferCopyRegion;
    }

    void image_copyFromBuffer( VkCommandBuffer cmd,
                               ObjectID_t<Image>  dstImage,
                               ObjectID_t<Buffer> srcBuffer,
                               std::vector<VkBufferImageCopy> const & copyRegions)
    {
        auto & I = get(dstImage);
        auto & B = get(srcBuffer);

        vkCmdCopyBufferToImage(
            cmd,
            B._buffer,
            I._image,
            VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
            static_cast<uint32_t>(copyRegions.size()),
            copyRegions.data()
        );
    }

    void image_TransitionLayout(VkCommandBuffer cmd, ObjectID_t<Image> id, VkImageLayout oldLayout, VkImageLayout newLayout)
    {
        auto & I = get(id);
        image_TransitionLayout(cmd, id, oldLayout, newLayout, 0, I.imageCreateInfo.arrayLayers, 0, I.imageCreateInfo.mipLevels);
    }

    void image_TransitionLayout(VkCommandBuffer cmd, ObjectID_t<Image> id, VkImageLayout oldLayout, VkImageLayout newLayout, uint32_t layer, uint32_t miplevel)
    {
        image_TransitionLayout(cmd, id, oldLayout, newLayout, layer, 1, miplevel, 1);
    }

    void image_TransitionLayout(VkCommandBuffer cmd,
                                ObjectID_t<Image> id,
                                VkImageLayout oldLayout,
                                VkImageLayout newLayout,
                                uint32_t firstLayer, uint32_t layerCount,
                                uint32_t firstMip  , uint32_t mipCount)
    {
        auto & I = get(id);

        VkImageSubresourceRange subresourceRange = {};
        subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;

        subresourceRange.baseMipLevel = firstMip;
        subresourceRange.levelCount   = mipCount;

        subresourceRange.baseArrayLayer = firstLayer;
        subresourceRange.layerCount     = layerCount;

        // Image barrier for optimal image (target)
        // Optimal image will be used as destination for the copy
        {
            VkImageMemoryBarrier imageMemoryBarrier{};
            imageMemoryBarrier.sType         = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
            imageMemoryBarrier.oldLayout     = oldLayout;
            imageMemoryBarrier.newLayout     = newLayout;
            imageMemoryBarrier.srcAccessMask = 0;
            imageMemoryBarrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
            imageMemoryBarrier.image         = I._image;
            imageMemoryBarrier.subresourceRange = subresourceRange;

            vkCmdPipelineBarrier(cmd, VK_PIPELINE_STAGE_ALL_COMMANDS_BIT, VK_PIPELINE_STAGE_ALL_COMMANDS_BIT, 0, 0, nullptr, 0, nullptr, 1, &imageMemoryBarrier);
        }
    }

    void imageCopyData(ObjectID_t<Image> img, void const * data, size_t byteSize)
    {
        auto stagingBuffer = buffer_Create(byteSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VMA_MEMORY_USAGE_CPU_TO_GPU);
        auto mapped = bufferMapData(stagingBuffer);
        std::memcpy(mapped, data, byteSize);

        bufferFlushData(stagingBuffer,0, byteSize);
        bufferUnmapData(stagingBuffer);

        auto & IC = get(img);

        auto w= IC.imageCreateInfo.extent.width;
        auto h= IC.imageCreateInfo.extent.height;
        VkExtent2D extent{ w, h};

        beginCommandBuffer([&](auto cmd)
        {
            image_TransitionLayout(cmd, img, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);

            image_copyFromBuffer2(cmd,

                                          stagingBuffer,                  // src buffer
                                          0,                   // buffer offset for pixel (0,0,0) is
                                          extent, // width/height of the buffer image
                                          4,                   // bytes per pixel
                                          VkOffset2D{0,0},     // copy from offset 2,2

                                          img,                 // dst Image
                                          0,0,                  // into layer/mip level
                                          VkOffset2D{0,0},      // copy to offset 2x2 on mip level 1
                                          extent); // copy a 2x2 region over


           image_TransitionLayout(cmd, img, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
        });

        buffer_Destroy(stagingBuffer);

    }


    void imageCopyIntoLayer(VkCommandBuffer cmd,
                            ObjectID_t<Image> dstImage,
                            uint32_t dstLayerlayer,

                            ObjectID_t<Buffer> srcStagingBuffer,
                            uint32_t           srcImageOffset,
                            VkExtent2D         srcBufferImageExtent)
    {
        image_copyFromBuffer2(cmd,

                              srcStagingBuffer,                  // src buffer
                              srcImageOffset,                   // buffer offset for pixel (0,0,0) is
                              srcBufferImageExtent, // width/height of the buffer image
                              4,                   // bytes per pixel
                              VkOffset2D{0,0},     // copy from offset 2,2

                              dstImage,                 // dst Image
                              dstLayerlayer,0,                  // into layer/mip level
                              VkOffset2D{0,0},      // copy to offset 2x2 on mip level 1
                              srcBufferImageExtent); // copy a 2x2 region over
    }
    /**
     * @brief getDescriptorInfo
     * @param id
     * @return
     *
     * Returns a DescriptorBufferInfo object which can be used to
     * update the a descriptor set with this buffer.
     * It always returns the full buffer.
     */
    vk::DescriptorBufferInfo getDescriptorInfo(ObjectID_t<Buffer> id) const
    {
        vk::DescriptorBufferInfo bi;
        auto & G = get(id);
        bi.buffer = G._buffer;
        bi.offset = 0;
        bi.range  = G._requestedSize - bi.offset;
        return bi;
    }

    /**
     * @brief getDescriptorInfo
     * @param id
     * @param nearest
     * @return
     *
     * Returns a DescriptorImageInfo object which can be used to update a descriptor
     * set with this image. if nearest is set to true, it will return the sampler
     * for NEAREST sampling instead of linear.
     */
    vk::DescriptorImageInfo getDescriptorInfo(ObjectID_t<Image> id, bool nearest=false) const
    {
        vk::DescriptorImageInfo bi;
        auto & G = get(id);
        bi.sampler = nearest ? G.sampler.nearest : G.sampler.linear;
        bi.imageLayout = vk::ImageLayout::eShaderReadOnlyOptimal;
        bi.imageView = G._view;
        return bi;
    }

    VkImageView getImageView( ObjectID_t<Image> id)
    {
        auto & U = get(id);
        return U._view;
    }
    VkSampler getLinearSampler( ObjectID_t<Image> id)
    {
        auto & U = get(id);
        return U.sampler.linear;
    }
    VkSampler getNearestSampler( ObjectID_t<Image> id)
    {
        auto & U = get(id);
        return U.sampler.nearest;
    }
};


/**
 * @brief The GLTFAsset struct
 *
 * The GLTFAsset is the top level class which holds
 * the entire scene to be rendered.
 */
struct Instance : public gvk::VulkanL2
{
    /**
     * @brief allocatePrimitive
     * @param P
     * @return
     *
     * Allocates enough data on the GPU to hold the primtive data.
     * All vertex data is stored as a single buffer
     *
     * B : [  <...positions...> <...normals...> <....> <...indices...> ]
     */
    gvk::ObjectID_t<Primitive> allocatePrimitive(gul::MeshPrimitive const &P, bool copyData=false)
    {
        VkDeviceSize size = P.calculateDeviceSize();

        spdlog::info("Allocating buffer for primitive: {} ", size);

        auto b = buffer_Create(size, VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT, VMA_MEMORY_USAGE_GPU_ONLY );

        auto pA = make_handle<Primitive>();
        auto & A  = get(pA);
        A.buffer  = b;

        if( copyData)
        {
            primitiveCopyData(pA, P);
        }

        return pA;
    };

    /**
     * @brief primitiveCopyData
     * @param pID
     * @param P
     *
     * Copy primitive data
     */
    void primitiveCopyData(gvk::ObjectID_t<Primitive> pID, gul::MeshPrimitive const & P)
    {
        auto & Pr = get(pID);

        auto byteSize = get(Pr.buffer)._requestedSize;

        assert( P.calculateDeviceSize() <= byteSize);

        auto stagingBuffer = buffer_Create(byteSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VMA_MEMORY_USAGE_CPU_TO_GPU);

        auto mapped = bufferMapData(stagingBuffer);

        std::vector<gul::VertexAttribute_v const*> V;
        auto offsets = gul::VertexAttributeCopySequential(mapped,
                                                          {
                                                              &P.POSITION  ,
                                                              &P.NORMAL    ,
                                                              &P.TANGENT   ,
                                                              &P.TEXCOORD_0,
                                                              &P.TEXCOORD_1,
                                                              &P.COLOR_0   ,
                                                              &P.JOINTS_0  ,
                                                              &P.WEIGHTS_0 ,
                                                              &P.INDEX
                                                          }
                                                          );// P.copyData(mapped);



        uint32_t vertexAttributeMask = 0;

        if( gul::VertexAttributeCount(P.POSITION  )==0) { offsets[0] = 0;} else { vertexAttributeMask |= (1u << 0u); }
        if( gul::VertexAttributeCount(P.NORMAL    )==0) { offsets[1] = 0;} else { vertexAttributeMask |= (1u << 1u); }
        if( gul::VertexAttributeCount(P.TANGENT   )==0) { offsets[2] = 0;} else { vertexAttributeMask |= (1u << 2u); }
        if( gul::VertexAttributeCount(P.TEXCOORD_0)==0) { offsets[3] = 0;} else { vertexAttributeMask |= (1u << 3u); }
        if( gul::VertexAttributeCount(P.TEXCOORD_1)==0) { offsets[4] = 0;} else { vertexAttributeMask |= (1u << 4u); }
        if( gul::VertexAttributeCount(P.COLOR_0   )==0) { offsets[5] = 0;} else { vertexAttributeMask |= (1u << 5u); }
        if( gul::VertexAttributeCount(P.JOINTS_0  )==0) { offsets[6] = 0;} else { vertexAttributeMask |= (1u << 6u); }
        if( gul::VertexAttributeCount(P.WEIGHTS_0 )==0) { offsets[7] = 0;} else { vertexAttributeMask |= (1u << 7u); }
        if( gul::VertexAttributeCount(P.INDEX     )==0) { offsets[8] = 0;} else { vertexAttributeMask |= (1u << 8u); }

        Pr.vertexAttributeMask = vertexAttributeMask;
        Pr.vertexOffsets = offsets;
        Pr.indexOffset   = offsets.back();
        Pr.vertexOffsets.pop_back();

        Pr.indexCount  = static_cast<uint32_t>(gul::VertexAttributeCount(P.INDEX));
        Pr.vertexCount = static_cast<uint32_t>(gul::VertexAttributeCount(P.POSITION));

        bufferFlushData(stagingBuffer,0, byteSize);
        bufferUnmapData(stagingBuffer);

        {
            auto & S = get(stagingBuffer);
            auto & D = get(Pr.buffer);

            auto fence = beginCommandBuffer([&](VkCommandBuffer cmd)
            {
                VkBufferCopy copyRegion = {};

                copyRegion.dstOffset = 0;
                copyRegion.srcOffset = 0;
                copyRegion.size      = byteSize;

                vkCmdCopyBuffer(cmd, S._buffer, D._buffer, 1, &copyRegion);
            });

        //    fence->wait();
        //    fence->destroy();
        }

        buffer_Destroy(stagingBuffer);

        if(gul::VertexAttributeCount(P.INDEX) > 0)
        {
            Pr.indexType =
            gul::VertexAttributeSizeOf(P.INDEX) == 4  ?
                        VK_INDEX_TYPE_UINT32 : VK_INDEX_TYPE_UINT16;
        }
    }

};


}

#endif
