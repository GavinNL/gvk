#ifndef GVK_OBJECT_ID_H
#define GVK_OBJECT_ID_H

#include <vulkan/vulkan.h>
#include <vector>
#include <cstdint>
#include <exception>
#include <stdexcept>
#include <cassert>
#include <entt/entt.hpp>

namespace gvk
{

enum class HandleID: uint32_t {};

template<typename T>
struct ObjectID_t
{
    using object_type = T;
    using handle_entity_type = HandleID;

    bool operator < ( ObjectID_t<T> const &D) const
    {
        return m_entity < D.m_entity;
    }
    bool operator == ( ObjectID_t<T> const &D) const
    {
        return m_entity == D.m_entity;
    }
    bool operator != ( ObjectID_t<T> const &D) const
    {
        return m_entity != D.m_entity;
    }
    operator bool() const
    {
        return m_entity != entt::null;
    }

    HandleID getEntity() const
    {
        return m_entity;
    }

    HandleID m_entity = entt::null;
};

}

#endif
