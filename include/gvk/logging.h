#ifndef GVK_LOGGING_H
#define GVK_LOGGING_H

#if __has_include(<spdlog/spdlog.h>)

  #include <spdlog/spdlog.h>

  #define LDEBUG(...) spdlog::info( __VA_ARGS__ )
  #define LINFO(...) spdlog::info( __VA_ARGS__ )
  #define LERROR(...) spdlog::error( __VA_ARGS__ )
  #define LWARN(...) spdlog::warn( __VA_ARGS__ )

#else
  #define LDEBUG(...)
  #define LINFO(...)
  #define LERROR(...)
  #define LWARN(...)
#endif


#endif
