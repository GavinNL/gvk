#ifndef GVK_OBJECT_PRIMITIVE_H
#define GVK_OBJECT_PRIMITIVE_H

#include "Buffer.h"
#include <vector>
#include <optional>
#include "../HandleContainer.h"

namespace gvk
{
struct Primitive
{
    gvk::ObjectID_t<gvk::Buffer> buffer;

    std::vector<VkDeviceSize>    vertexOffsets;
    VkDeviceSize                 indexOffset;
    VkIndexType                  indexType = VK_INDEX_TYPE_UINT32;

    uint32_t indexCount=0;
    uint32_t vertexCount=0;
    uint32_t vertexAttributeMask = 0;
};
}
#endif
