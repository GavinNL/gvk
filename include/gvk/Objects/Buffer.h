#ifndef GVK_OBJECT_BUFFER_H
#define GVK_OBJECT_BUFFER_H

#include <vulkan/vulkan.h>
#include <vk_mem_alloc.h>

namespace gvk
{
struct Buffer
{
    VkBufferCreateInfo createInfo;
    VkBuffer          _buffer;
    VmaAllocation     _allocation=nullptr;
    VmaAllocationInfo _allocInfo;
    VkDeviceSize      _requestedSize=0;
};
}
#endif
