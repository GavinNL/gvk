#ifndef GVK_OBJECT_PBRMATERIAL_H
#define GVK_OBJECT_PBRMATERIAL_H

#include <vector>
#include <optional>
#include <glm/glm.hpp>
#include "../ObjectID.h"
#include "Image.h"

namespace gvk
{
struct PBRMaterial
{
    glm::vec4 baseColorFactor    = {1,1,1,1};
    glm::vec4 emissiveFactor     = {0,0,0,0}; // only the RGB values are used

    float     metallicFactor     = 1.0f;
    float     roughnessFactor    = 1.0f;
    float     alphaCutoff        = 0.5f;
    float     vertexNormalOffset = 0.0f;

    struct
    {
        ObjectID_t<Image> baseColor;
        ObjectID_t<Image> normal;
        ObjectID_t<Image> metallicRoughness;
        ObjectID_t<Image> emissive;

    } textures;

    struct
    {
        std::string baseColor;
        std::string normal;
        std::string metallicRoughness;
        std::string emissive;

    } textureURI;

};

}
#endif
