#ifndef GVK_OBJECT_IMAGE_H
#define GVK_OBJECT_IMAGE_H

#include <vulkan/vulkan.h>
#include <vk_mem_alloc.h>

namespace gvk
{

struct Image
{
    VkImageCreateInfo imageCreateInfo;

    VkImage           _image;
    VkImageView       _view;
    VkImageViewType   _viewType;
    VmaAllocation     _allocation=nullptr;
    VmaAllocationInfo _allocInfo;

    struct
    {
        VkSampler nearest;
        VkSampler linear;
    } sampler;
};

}
#endif

