#include <iostream>
#include <cassert>

#include <vkw/VulkanApplication.h>
#include <vkw/SDLWidget.h>

#include <entt/entt.hpp>
#include <glm/glm.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/io.hpp>
#include <gvk/L1.h>

#include <vkb/utils/DynamicPipeline.h>
#include <vkb/utils/DescriptorSetAllocator.h>
#include <vkb/utils/DescriptorUpdater.h>
#include <vkb/utils/multistoragebuffer.h>
#include <vkb/utils/TextureArrayDescriptorSet.h>
#include <vkb/detail/ComputePipelineCreateInfo2.h>
#include <vkb/vkb.h>

#include <GLSLCompiler.h>

#include <filesystem>
#include <numeric>
#include <algorithm>
#include <future>

#include <spdlog/spdlog.h>

#include <gul/math/Transform.h>
#include <gul/MeshPrimitive.h>
#include <gul/ResourceLocator.h>
#include <gul/Image.h>
#include <stb_image.h>

#include <gvk/Objects/Primtive.h>
#include <gvk/Renderers/PBRPrimitiveRenderer.h>
#include <gvk/Renderers/PBRGenerator.h>
#include <gvk/Renderers/ImguiRenderer.h>

#include <imgui.h>
#include <imgui_internal.h>
#include <imgui_impl_vulkan.h>
#include <imgui_impl_sdl.h>
#include <imconfig.h>
#include <nlohmann/json.hpp>

SDL_Window * g_window = nullptr;


struct QwickTimer
{
    std::chrono::system_clock::time_point m_start = std::chrono::system_clock::time_point();
    ~QwickTimer()
    {
        auto now = std::chrono::system_clock::time_point();
        std::cout << std::chrono::duration<double>(now-m_start).count() << "\n";
    }
};

struct CameraController
{

    static void CameraLook(glm::quat & rotation,
                            glm::vec3 & angularVelocity,
                            float       mouseSens = 2.0f,
                            float       decay = 15.0f)
    {
        auto & io = ImGui::GetIO();

        auto dx = -io.MouseDelta.x * mouseSens;
        auto dy = -io.MouseDelta.y * mouseSens;

        if( io.MouseDown[1] )
        {
            angularVelocity.y += io.DeltaTime * dx;
            angularVelocity.x += io.DeltaTime * dy;
        }

        gul::Transform T;
        T.rotation = rotation;

        T.rotateGlobal( {0,1,0}, angularVelocity.y * io.DeltaTime);
        T.rotateLocal(  {1,0,0}, angularVelocity.x * io.DeltaTime);

        rotation        = T.rotation;
        angularVelocity *= 1 - decay*io.DeltaTime;
    }
    static void CameraMove(glm::vec3 & position,
                           glm::vec3 & velocity,
                           glm::quat const& rotation,
                           float acceleration = 40.0f,
                           float drag = 10.0f)
    {
        auto A = acceleration;

        auto & io = ImGui::GetIO();

        glm::vec3 a(0,0,0);

        if( io.KeysDown[SDL_SCANCODE_W] )
        {
            a += glm::vec3(0,0,A);
        }
        if( io.KeysDown[SDL_SCANCODE_S] )
        {
            a += glm::vec3(0,0,-A);
        }
        if( io.KeysDown[SDL_SCANCODE_A] )
        {
            a += glm::vec3(A,0,0);
        }
        if( io.KeysDown[SDL_SCANCODE_D] )
        {
            a += glm::vec3(-A,0,0);
        }

        float alpha = drag;

        a = rotation * a;
        const auto t = io.DeltaTime;
        auto & v = velocity;
        auto & p = position;

        // drag force
        auto d = glm::length(v) * v * alpha;

        v += (a - d) * t;
        p += v * t + 0.5f*a*t*t;
        v *= 1 - (1.f - glm::step( 0.1f , glm::length(v) ) )*0.8;

    }
};


class MyApplication : public vkw::Application
{
    // Application interface
public:
    gul::ResourceLocator            m_ResourceLocator;
    gvk::Instance                   m_asset;
    vkb::Storage                    m_storage;

    gvk::ImguiRenderer              m_GUI;
    gvk::PBRPrimitiveRenderer       m_renderer;
    gvk::PBRTextureGenerator        m_generator;

    struct
    {
        gvk::ObjectID_t<gvk::PBRMaterial> red;
        gvk::ObjectID_t<gvk::PBRMaterial> green;
        gvk::ObjectID_t<gvk::PBRMaterial> blue;
    } m_materials;

    gvk::ObjectID_t<gvk::Primitive> m_primitive;
    gvk::ObjectID_t<gvk::Image>     i_Img;

    gvk::ObjectID_t<gvk::Image>     m_envMap;
    gvk::ObjectID_t<gvk::Image>     m_IRRMap;
    gvk::ObjectID_t<gvk::Image>     m_BRDF;

    gul::Image loadImage(const std::string & resource) const
    {
        auto binaryArray = m_ResourceLocator.readResourceBIN(resource);


        int width, height, channels;
        int desired_channels = 4;

        auto raw =
        stbi_load_from_memory(binaryArray.data(),
                              static_cast<int>(binaryArray.size()),
                              &width,
                              &height,
                              &channels,
                              desired_channels);

        gul::Image I;
        I.resize( static_cast<uint32_t>(width) , static_cast<uint32_t>(height), 4 );
        I.copyFromBuffer(raw,
                         static_cast<uint32_t>(width*height*desired_channels),
                         static_cast<uint32_t>(width) ,
                         static_cast<uint32_t>(height),
                         static_cast<uint32_t>(desired_channels));

        stbi_image_free(raw);
        return I;
    }

    gvk::ObjectID_t<gvk::Image> createImageFromResource(const std::string & resource)
    {
        auto Himg  = loadImage(resource);
        auto imgID = m_asset.image2D_Create( Himg.width(), Himg.height(), VK_FORMAT_R8G8B8A8_UNORM, 1, 1);
        m_asset.imageCopyData(imgID, Himg.data(), Himg.size());
        return imgID;
    }

    gvk::ObjectID_t< gvk::PBRMaterial > loadMaterial(std::string const & resource)
    {
        auto string = m_ResourceLocator.readResourceASCII(resource);

        nlohmann::json J = nlohmann::json::parse(string);
        std::cout << J.dump(4) << std::endl;

        std::vector<std::string> imageURI;
        std::vector<uint32_t>    textures;

        #define EXTENSION_NAME_ "EXTERNAL_"
        if( J.count("extensions"))
        {
            if( J.at("extensions").count(EXTENSION_NAME_))
            {
                auto & E = J.at("extensions").at(EXTENSION_NAME_);
                for(auto & image : E.at("images"))
                {
                    imageURI.push_back( image.at("uri").get<std::string>());
                }
                for(auto & tex : E.at("textures"))
                {
                    textures.push_back( tex.at("source").get<uint32_t>());
                }
            }
        }

        gvk::PBRMaterial M;

        if( J.count("pbrMetallicRoughness"))
        {
            auto & pbrMetallicRoughness = J.at("pbrMetallicRoughness");
            auto baseC = pbrMetallicRoughness.value("baseColorFactor", std::array<float,4>({1,1,1,1}));
            M.baseColorFactor[0] = baseC[0];
            M.baseColorFactor[1] = baseC[1];
            M.baseColorFactor[2] = baseC[2];
            M.baseColorFactor[3] = baseC[3];

            M.roughnessFactor = pbrMetallicRoughness.value("roughnessFactor", 1.0f);
            M.metallicFactor  = pbrMetallicRoughness.value("metallicFactor", 1.0f);

            if( pbrMetallicRoughness.count("baseColorTexture"))
            {
                auto & normalTexture = pbrMetallicRoughness.at("baseColorTexture");
                auto index = normalTexture.at("index").get<uint32_t>();

                M.textureURI.baseColor = imageURI[ textures[index] ];
            }

            if( pbrMetallicRoughness.count("metallicRoughnessTexture"))
            {
                auto & normalTexture = pbrMetallicRoughness.at("metallicRoughnessTexture");
                auto index = normalTexture.at("index").get<uint32_t>();

                M.textureURI.metallicRoughness = imageURI[ textures[index] ];
            }
        }

        if( J.count("normalTexture"))
        {
            auto & normalTexture = J.at("normalTexture");
            auto index = normalTexture.at("index").get<uint32_t>();

            M.textureURI.normal = imageURI[ textures[index] ];
        }
        if( J.count("emissiveTexture"))
        {
            auto & normalTexture = J.at("emissiveTexture");
            auto index = normalTexture.at("index").get<uint32_t>();

            M.textureURI.emissive = imageURI[ textures[index] ];
        }

        auto id = m_asset.make_handle<gvk::PBRMaterial>();
        auto & G = m_asset.get(id);
        G = M;

        if( G.textureURI.baseColor.size())
        {
            G.textures.baseColor = createImageFromResource(G.textureURI.baseColor);
        }
        if( G.textureURI.normal.size())
        {
            G.textures.normal = createImageFromResource(G.textureURI.normal);
        }
        //M.textures.normal    = createImageFromResource("textures/metal/gold/normal.jpg");
        //return M;
        return id;
    }


    void initResources() override
    {
        m_ResourceLocator.push_back( GVK_SOURCE_DIR "/share");
        glslang::InitializeProcess();

        m_asset.initVulkan( getInstance(), getPhysicalDevice(), getDevice(), getGraphicsQueue());

        {
            loadMaterial("textures/metal/gold/gold.json");
        }
        {
            auto P = gul::Sphere(0.5f,40,40);
            //auto P = gul::Box(1,1,1);

            m_primitive = m_asset.allocatePrimitive(P, true);
        }

        {
            m_materials.red = loadMaterial("textures/metal/gold/gold.json");;//m_asset.make_handle<gvk::PBRMaterial>();
            //auto &  M = m_asset.get(m_materials.red);
            //M.baseColorFactor = {1,0,0.0f,1.0f};
            //M.metallicFactor = 1.0f;
            //M.roughnessFactor = 0.0f;
        }
        {
            m_materials.green = m_asset.make_handle<gvk::PBRMaterial>();
            auto &  M = m_asset.get(m_materials.green);
            M.baseColorFactor = {0,1,0.0f,1.0f};
            M.roughnessFactor = 0.0f;
        }
        {
            m_materials.blue= m_asset.make_handle<gvk::PBRMaterial>();
            auto &  M = m_asset.get(m_materials.blue);
            M.baseColorFactor = {0,0,1.0f,1.0f};
            M.roughnessFactor = 0.0f;
        }


        // Create the Storage buffers that we will be using

        {
            m_generator.initResources(getDevice(), &m_asset, &m_ResourceLocator);
        }

        {
            gvk::PBRPrimitiveRenderer::InitInfo I;
            I.device           = getDevice();
            I.registry         = &m_asset;
            I.resourceLocator  = &m_ResourceLocator;
            I.textureGenerator = &m_generator; // PBR renderer needs the Generator to generate env maps and the BRDF
            m_renderer.initResources(I);
        }

        if(1)
        {
            auto Himg = loadImage("textures/textures.jpg");
            spdlog::info("Image loaded: {}x{}", Himg.width(), Himg.height());
            i_Img    = m_asset.image2D_Create( Himg.width(),Himg.height(), VK_FORMAT_R8G8B8A8_UNORM, 1, 1);

            m_asset.imageCopyData(i_Img, Himg.data(), Himg.size());
        }
        // The following can be used here
        // getDevice();
        // getPhysicalDevice();
        // getInstance();
        //getGraphicsQueue();

        {
            spdlog::info("Start");
            auto Himg = loadImage("textures/panorama/pano-finse-autumn_1024/radiance_0.jpg");
            spdlog::info("texture loaded from file system");

            auto imgID     = m_asset.image2D_Create( Himg.width(), Himg.height(), VK_FORMAT_R8G8B8A8_UNORM, 1, 1);
            m_asset.imageCopyData(imgID, Himg.data(), Himg.size());

            m_renderer.generateNewEnvironment(imgID);
            m_asset.image_Destroy(imgID);
        }

        std::cout << "initResources() " << std::endl;
    }

    void releaseResources() override
    {
        m_renderer.releaseResources();
        m_GUI.releaseResources();
        m_generator.releaseResources();
        m_storage.destroyAll(getDevice());
        m_asset.shutdown();

        // The following can be used here
        // getDevice();
        // getPhysicalDevice();
        // getInstance();
        std::cout << "releaseResources() " << std::endl;
        glslang::FinalizeProcess();
    }

    void initSwapChainResources() override
    {
        // The following can be used here
        // swapchainImageCount();
        // swapchainImage( index );
        // colorFormat();
        // depthStencilFormat();
        // swapchainImageSize();
        // swapchainImageView();

        std::cout << "initSwapchainResources() " << std::endl;
    }

    void releaseSwapChainResources() override
    {
        std::cout << "releaseSwapChainResources() " << std::endl;
    }

    void render( vkw::Frame &frame) override
    {
        auto start_t = std::chrono::system_clock::now();
        static bool once=true;
        if(once)
        {
            once = false;

            gvk::ImguiRenderer::InitInfo info;

            uint32_t g_MinImageCount = 2;

            info.storage         = &m_storage;
            info.registry        = &m_asset;
            info.resourceLocator = &m_ResourceLocator;
            info.Instance        = getInstance();
            info.PhysicalDevice  = getPhysicalDevice();
            info.Device          = getDevice();
            info.QueueFamily     = getGraphicsQueueFamilyIndex();
            info.Queue           = getGraphicsQueue();
            //info.PipelineCache   = NULL;
            //info.DescriptorPool  = g_DescriptorPool;
            //info.Allocator       = NULL;
            info.MinImageCount   = g_MinImageCount;
            info.ImageCount      = swapchainImageCount();
            info.RenderPass      = frame.renderPass;

            m_GUI.initResources(info);

            {
                ImVec4* colors = ImGui::GetStyle().Colors;
                colors[ImGuiCol_Text]                   = ImVec4(1.00f, 1.00f, 1.00f, 1.00f);
                colors[ImGuiCol_TextDisabled]           = ImVec4(0.50f, 0.50f, 0.50f, 1.00f);
                colors[ImGuiCol_WindowBg]               = ImVec4(0.06f, 0.06f, 0.06f, 0.94f);
                colors[ImGuiCol_ChildBg]                = ImVec4(0.00f, 0.00f, 0.00f, 0.00f);
                colors[ImGuiCol_PopupBg]                = ImVec4(0.08f, 0.08f, 0.08f, 0.94f);
                colors[ImGuiCol_Border]                 = ImVec4(0.43f, 0.43f, 0.50f, 0.50f);
                colors[ImGuiCol_BorderShadow]           = ImVec4(0.00f, 0.00f, 0.00f, 0.00f);
                colors[ImGuiCol_FrameBg]                = ImVec4(0.44f, 0.44f, 0.44f, 0.60f);
                colors[ImGuiCol_FrameBgHovered]         = ImVec4(0.57f, 0.57f, 0.57f, 0.70f);
                colors[ImGuiCol_FrameBgActive]          = ImVec4(0.76f, 0.76f, 0.76f, 0.80f);
                colors[ImGuiCol_TitleBg]                = ImVec4(0.04f, 0.04f, 0.04f, 1.00f);
                colors[ImGuiCol_TitleBgActive]          = ImVec4(0.16f, 0.16f, 0.16f, 1.00f);
                colors[ImGuiCol_TitleBgCollapsed]       = ImVec4(0.00f, 0.00f, 0.00f, 0.60f);
                colors[ImGuiCol_MenuBarBg]              = ImVec4(0.14f, 0.14f, 0.14f, 1.00f);
                colors[ImGuiCol_ScrollbarBg]            = ImVec4(0.02f, 0.02f, 0.02f, 0.53f);
                colors[ImGuiCol_ScrollbarGrab]          = ImVec4(0.31f, 0.31f, 0.31f, 1.00f);
                colors[ImGuiCol_ScrollbarGrabHovered]   = ImVec4(0.41f, 0.41f, 0.41f, 1.00f);
                colors[ImGuiCol_ScrollbarGrabActive]    = ImVec4(0.51f, 0.51f, 0.51f, 1.00f);
                colors[ImGuiCol_CheckMark]              = ImVec4(0.13f, 0.75f, 0.55f, 0.80f);
                colors[ImGuiCol_SliderGrab]             = ImVec4(0.13f, 0.75f, 0.75f, 0.80f);
                colors[ImGuiCol_SliderGrabActive]       = ImVec4(0.13f, 0.75f, 1.00f, 0.80f);
                colors[ImGuiCol_Button]                 = ImVec4(0.13f, 0.75f, 0.55f, 0.40f);
                colors[ImGuiCol_ButtonHovered]          = ImVec4(0.13f, 0.75f, 0.75f, 0.60f);
                colors[ImGuiCol_ButtonActive]           = ImVec4(0.13f, 0.75f, 1.00f, 0.80f);
                colors[ImGuiCol_Header]                 = ImVec4(0.13f, 0.75f, 0.55f, 0.40f);
                colors[ImGuiCol_HeaderHovered]          = ImVec4(0.13f, 0.75f, 0.75f, 0.60f);
                colors[ImGuiCol_HeaderActive]           = ImVec4(0.13f, 0.75f, 1.00f, 0.80f);
                colors[ImGuiCol_Separator]              = ImVec4(0.13f, 0.75f, 0.55f, 0.40f);
                colors[ImGuiCol_SeparatorHovered]       = ImVec4(0.13f, 0.75f, 0.75f, 0.60f);
                colors[ImGuiCol_SeparatorActive]        = ImVec4(0.13f, 0.75f, 1.00f, 0.80f);
                colors[ImGuiCol_ResizeGrip]             = ImVec4(0.13f, 0.75f, 0.55f, 0.40f);
                colors[ImGuiCol_ResizeGripHovered]      = ImVec4(0.13f, 0.75f, 0.75f, 0.60f);
                colors[ImGuiCol_ResizeGripActive]       = ImVec4(0.13f, 0.75f, 1.00f, 0.80f);
                colors[ImGuiCol_Tab]                    = ImVec4(0.13f, 0.75f, 0.55f, 0.80f);
                colors[ImGuiCol_TabHovered]             = ImVec4(0.13f, 0.75f, 0.75f, 0.80f);
                colors[ImGuiCol_TabActive]              = ImVec4(0.13f, 0.75f, 1.00f, 0.80f);
                colors[ImGuiCol_TabUnfocused]           = ImVec4(0.18f, 0.18f, 0.18f, 1.00f);
                colors[ImGuiCol_TabUnfocusedActive]     = ImVec4(0.36f, 0.36f, 0.36f, 0.54f);
                //colors[ImGuiCol_DockingPreview]         = ImVec4(0.13f, 0.75f, 0.55f, 0.80f);
                //colors[ImGuiCol_DockingEmptyBg]         = ImVec4(0.13f, 0.13f, 0.13f, 0.80f);
                colors[ImGuiCol_PlotLines]              = ImVec4(0.61f, 0.61f, 0.61f, 1.00f);
                colors[ImGuiCol_PlotLinesHovered]       = ImVec4(1.00f, 0.43f, 0.35f, 1.00f);
                colors[ImGuiCol_PlotHistogram]          = ImVec4(0.90f, 0.70f, 0.00f, 1.00f);
                colors[ImGuiCol_PlotHistogramHovered]   = ImVec4(1.00f, 0.60f, 0.00f, 1.00f);
                colors[ImGuiCol_TableHeaderBg]          = ImVec4(0.19f, 0.19f, 0.20f, 1.00f);
                colors[ImGuiCol_TableBorderStrong]      = ImVec4(0.31f, 0.31f, 0.35f, 1.00f);
                colors[ImGuiCol_TableBorderLight]       = ImVec4(0.23f, 0.23f, 0.25f, 1.00f);
                colors[ImGuiCol_TableRowBg]             = ImVec4(0.00f, 0.00f, 0.00f, 0.00f);
                colors[ImGuiCol_TableRowBgAlt]          = ImVec4(1.00f, 1.00f, 1.00f, 0.07f);
                colors[ImGuiCol_TextSelectedBg]         = ImVec4(0.26f, 0.59f, 0.98f, 0.35f);
                colors[ImGuiCol_DragDropTarget]         = ImVec4(1.00f, 1.00f, 0.00f, 0.90f);
                colors[ImGuiCol_NavHighlight]           = ImVec4(0.26f, 0.59f, 0.98f, 1.00f);
                colors[ImGuiCol_NavWindowingHighlight]  = ImVec4(1.00f, 1.00f, 1.00f, 0.70f);
                colors[ImGuiCol_NavWindowingDimBg]      = ImVec4(0.80f, 0.80f, 0.80f, 0.20f);
                colors[ImGuiCol_ModalWindowDimBg]       = ImVec4(0.80f, 0.80f, 0.80f, 0.35f);
            }
        }

        if(1)
        {
            static int cc=0;
            cc++;

            if( cc == 1000)
            {
                m_ImageFuture = std::async( std::launch::async,[this]()
                {
                    spdlog::info("Loading new environment in the background");
                    return this->loadImage("textures/panorama/ocean_2048/radiance_3.jpg");
                });
            }        
            if( m_ImageFuture.valid() )
            {
                auto status = m_ImageFuture.wait_for(std::chrono::milliseconds(0));
                if (status == std::future_status::ready)
                {
                    auto Himg = m_ImageFuture.get();
                    auto imgID     = m_asset.image2D_Create( Himg.width(), Himg.height(), VK_FORMAT_R8G8B8A8_UNORM, 1, 1);
                    m_asset.imageCopyData(imgID, Himg.data(), Himg.size());

                    m_renderer.generateNewEnvironment(imgID);
                    m_asset.image_Destroy(imgID);
                    spdlog::info("Environment generated");
                    std::cout << "ready!\n";
                }
            }
        }


        assert( frame.depthImage != VK_NULL_HANDLE);

        frame.clearColor.float32[0] = 0.3f;
        frame.clearColor.float32[1] = 0.3f;
        frame.clearColor.float32[2] = 0.3f;
        frame.clearColor.float32[3] = 1.0f;

        frame.beginRenderPass( frame.commandBuffer );

            {
                gvk::PBRPrimitiveRenderer::BeginInfo B;
                B.commandBuffer = frame.commandBuffer;
                B.renderPass    = frame.renderPass;
                B.mousePos      = {ImGui::GetIO().MousePos.x, ImGui::GetIO().MousePos.y};//
                m_renderer.beginRender(B);


                m_renderer.setViewPort({0.f,0.f, static_cast<float>(frame.swapchainSize.width), static_cast<float>(frame.swapchainSize.height), 0 ,1.f});
                m_renderer.setScissor( { {0,0}, frame.swapchainSize } );

                {
                    static gul::Transform CameraT = gul::Transform({0,0,1});
                    static glm::vec3 velocity = glm::vec3(0,0,0);
                    static glm::vec3 angular_velocity = glm::vec3(0,0,0);

                    CameraController::CameraLook(CameraT.rotation, angular_velocity);
                    CameraController::CameraMove(CameraT.position, velocity        , CameraT.rotation);

                    auto proj   = glm::perspectiveFov( glm::radians(45.0f), static_cast<float>(frame.swapchainSize.width), static_cast<float>(frame.swapchainSize.height), 0.1f, 100.0f);
                    proj[1][1] *= -1;

                    auto V = CameraT.getViewMatrix();
                    m_renderer.setViewMatrix(V);
                    m_renderer.setProjMatrix(proj);
                    m_renderer.setProjViewMatrix( proj*V);
                }

                {
                    m_renderer.bindPrimitive(m_primitive);

                    uint32_t id=1;
                    gvk::ObjectID_t<gvk::PBRMaterial> mats[] = { m_materials.red, m_materials.green, m_materials.blue};
                    m_renderer.setMaterial( mats[0],0);

                    static std::vector<glm::mat4> modelMatrices = {};
                    modelMatrices.clear();

                    for(int i=0;i<5;i++)
                    {
                        for(int j=0;j<5;j++)
                        {                          
                            m_renderer.setModelMatrix( gul::Transform({i,0,j}).getMatrix());
                            m_renderer.setEntityID(id++);
                            m_renderer.setMaterial( mats[id%3],0);
                            m_renderer.draw(1);
                        }
                    }
                }

                m_renderer.endRender();

            }


        {
            ImGui_ImplSDL2_NewFrame(g_window);
            // feed inputs to dear imgui, start new frame
            m_GUI.begin(frame.commandBuffer);

            //ImGui::ShowStyleEditor();
            //ImGui::StyleColorsClassic();
            //ImGui::StyleColorsDark();

            showMaterialGUI(m_materials.red);

            if(0)
            {
                static float f = 0.0f;
                static int counter = 0;
                static ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);
                static bool show_demo_window=false;
                static bool show_another_window=false;
                ImGui::Begin("Hello, world!");                          // Create a window called "Hello, world!" and append into it.

                    ImGui::Text("This is some useful text.");               // Display some text (you can use a format strings too)
                    ImGui::Checkbox("Demo Window", &show_demo_window);      // Edit bools storing our window open/close state
                    ImGui::Checkbox("Another Window", &show_another_window);

                    ImGui::SliderFloat("float", &f, 0.0f, 1.0f);            // Edit 1 float using a slider from 0.0f to 1.0f
                    ImGui::ColorEdit3("clear color", (float*)&clear_color); // Edit 3 floats representing a color

                    if (ImGui::Button("Button"))                            // Buttons return true when clicked (most widgets return true when edited/activated)
                        counter++;
                    ImGui::SameLine();
                    ImGui::Text("counter = %d", counter);
                    ImGui::Text("Cmd Buff Time = %.3f", m_bufferRecordTime);

                    ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
                ImGui::End();
            }
            else
            {
                //static bool open=true;
                //ImGui::ShowDemoWindow(&open);
            }

            m_GUI.end();
        }

        frame.endRenderPass(frame.commandBuffer);

        // request the next frame
        // so that this function will be called again
        requestNextFrame();

        auto end_t = std::chrono::system_clock::now();
        m_bufferRecordTime = std::chrono::duration<float>(end_t-start_t).count();
    }
    float m_bufferRecordTime=0;

    void showMaterialGUI(gvk::ObjectID_t<gvk::PBRMaterial> id)
    {
        auto & M = m_asset.get(id);

        //ImGui::SetNextWindowPos( {0,0}, ImGuiCond_FirstUseEver);
        ImGui::SetNextWindowPos( {0, 0},  ImGuiCond_FirstUseEver) ;//-- set window position x, y
        ImGui::SetNextWindowSize({300, 600}, ImGuiCond_Appearing)    ;//-- set window size w, h

        ImGui::Begin("Material Editor");                          // Create a window called "Hello, world!" and append into it.

            ImGui::SliderFloat("metallic" , &M.metallicFactor, 0.0f, 1.0f);            // Edit 1 float using a slider from 0.0f to 1.0f
            ImGui::SliderFloat("roughness", &M.roughnessFactor, 0.0f, 1.0f);            // Edit 1 float using a slider from 0.0f to 1.0f
            ImGui::ColorPicker4("clear color", (float*)&M.baseColorFactor[0]); // Edit 3 floats representing a color
            ImGui::Text("Mouse Hover ID %d", m_renderer.getMouseHoverID() );

            if( ImGui::CollapsingHeader("test"))
            {
                ImGui::SliderFloat("roughness", &M.roughnessFactor, 0.0f, 1.0f);            // Edit 1 float using a slider from 0.0f to 1.0f

                if( ImGui::TreeNode("tree node") )
                {
                    //ImGui::TreeNodeEx("tree node ex");
                    auto & S = ImGui::GetStyle();
                    ImGui::SliderFloat("rounding", &S.FrameRounding, 0.0f, 10.0f);            // Edit 1 float using a slider from 0.0f to 1.0f
                    ImGui::TreePop();

                    ImGui::Button("Button");
                    ImGui::SmallButton("SmallButton");

                    ButtonEx("This is my test button", {100,100}, ImGuiButtonFlags_MouseButtonDefault_);
                }
                ImGui::TreeNodeEx("tree node ex");
            }
        ImGui::End();
    }


    bool ButtonEx(const char* label, const ImVec2& size_arg, ImGuiButtonFlags flags)
    {
        ImGuiWindow* window = ImGui::GetCurrentWindow();
        if (window->SkipItems)
            return false;

        auto _sum = [](const auto & a, const auto & b)
        {
            return ImVec2(a.x+b.x,a.y+b.y);
        };
        auto _diff = [](const auto & a, const auto & b)
        {
            return ImVec2(a.x-b.x,a.y-b.y);
        };
        ImGuiContext& g = *GImGui;

        const ImGuiStyle& style = g.Style;
        const ImGuiID id = window->GetID(label);
        const ImVec2 label_size = ImGui::CalcTextSize(label, NULL, true);


        ImVec2 pos = window->DC.CursorPos;
        if ((flags & ImGuiButtonFlags_AlignTextBaseLine) && style.FramePadding.y < window->DC.CurrLineTextBaseOffset) // Try to vertically align buttons that are smaller/have no padding so that text baseline matches (bit hacky, since it shouldn't be a flag)
            pos.y += window->DC.CurrLineTextBaseOffset - style.FramePadding.y;
        ImVec2 size = ImGui::CalcItemSize(size_arg, label_size.x + style.FramePadding.x * 2.0f, label_size.y + style.FramePadding.y * 2.0f);

        const ImRect bb(pos, ImVec2(pos.x+size.x, pos.y+size.y));
        ImGui::ItemSize(size, style.FramePadding.y);
        if (!ImGui::ItemAdd(bb, id))
            return false;

        if (g.CurrentItemFlags & ImGuiItemFlags_ButtonRepeat)
            flags |= ImGuiButtonFlags_Repeat;
        bool hovered, held;
        bool pressed = ImGui::ButtonBehavior(bb, id, &hovered, &held, flags);

        // Render
        const ImU32 col = ImGui::GetColorU32((held && hovered) ? ImGuiCol_ButtonActive : hovered ? ImGuiCol_ButtonHovered : ImGuiCol_Button);
        ImGui::RenderNavHighlight(bb, id);
        ImGui::RenderFrame(bb.Min, bb.Max, col, true, style.FrameRounding);

        if (g.LogEnabled)
            ImGui::LogSetNextTextDecoration("[", "]");
        ImGui::RenderTextClipped(_sum(bb.Min , style.FramePadding),
                          _diff(bb.Max,style.FramePadding),
                          label, NULL, &label_size, style.ButtonTextAlign, &bb);

        // Automatically close popups
        //if (pressed && !(flags & ImGuiButtonFlags_DontClosePopups) && (window->Flags & ImGuiWindowFlags_Popup))
        //    CloseCurrentPopup();

        IMGUI_TEST_ENGINE_ITEM_INFO(id, label, window->DC.LastItemStatusFlags);
        return pressed;
    }

    std::future<gul::Image> m_ImageFuture;
};

static VKAPI_ATTR VkBool32 VKAPI_CALL VulkanReportFunc(
    VkDebugReportFlagsEXT flags,
    VkDebugReportObjectTypeEXT objType,
    uint64_t obj,
    size_t location,
    int32_t code,
    const char* layerPrefix,
    const char* msg,
    void* userData)
{
    std::cout << "VULKAN VALIDATION: " << layerPrefix << " :: " <<  msg << std::endl;;

    return VK_FALSE;
}


int main()
{
    // This needs to be called first to initialize SDL
    SDL_Init(SDL_INIT_EVERYTHING);

    // create a vulkan window widget
    vkw::SDLVulkanWidget vulkanWindow;

    // set the initial properties of the
    // window. Also specify that we want
    // a depth stencil attachment
    vkw::SDLVulkanWidget::CreateInfo c;
    c.width       = 1024;
    c.height      = 768;
    c.windowTitle = "My Vulkan Application Window";
    c.depthFormat = VK_FORMAT_D32_SFLOAT_S8_UINT;
    c.callback    = &VulkanReportFunc;
    //c.enabledLayers.clear();
    // create the window and initialize
    c.enabledFeatures.fragmentStoresAndAtomics = true;
    vulkanWindow.create(c);


    // Setup Dear ImGui context
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();

    ImGui::GetIO().IniFilename = NULL;

    // Setup Platform/Renderer bindings
    ImGui_ImplSDL2_InitForVulkan(vulkanWindow.getSDLWindow());
    g_window = vulkanWindow.getSDLWindow();

    MyApplication app;

    // put the window in the main loop
    // and provide a callback function for the SDL events
    vulkanWindow.exec(&app,
                      [&app](SDL_Event const & evt)
    {
        ImGui_ImplSDL2_ProcessEvent(&evt);
        if( evt.type == SDL_QUIT)
            app.quit();
    });

    vulkanWindow.destroy();
    SDL_Quit();
    return 0;
}


#define VMA_IMPLEMENTATION
#include <vk_mem_alloc.h>
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#include <imgui_impl_vulkan.cpp>
#include <imgui_impl_sdl.cpp>
