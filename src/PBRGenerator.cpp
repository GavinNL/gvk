#include <vulkan/vulkan.h>
#include <vector>
#include <cstdint>
#include <exception>
#include <stdexcept>
#include <cassert>

#include <vkb/utils/DescriptorSetAllocator.h>
#include <vkb/detail/WriteDescriptorSet2.h>
#include <vkb/utils/SPIRV_DescriptorSetLayoutGenerator.h>
#include <vkb/detail/ComputePipelineCreateInfo2.h>
#include <vkb/vkb.h>

#include <gul/ResourceLocator.h>

#include <gvk/L1.h>

#include <spdlog/spdlog.h>

#include <gvk/Renderers/PBRGenerator.h>

namespace gvk
{

void PBRTextureGenerator::initResources(VkDevice _device, VulkanL2 *_registry, gul::ResourceLocator *_resourceLocator)
{
    device          = _device;
    registry        = _registry;
    resourceLocator = _resourceLocator;

    _initDSetAllocator();
    _initComputePipelines();
}

void PBRTextureGenerator::releaseResources()
{
    m_pipelines.dSetAllocator.destroy();

    m_storage.destroy(m_pipelines.rectToCube.pipeline, getDevice() );
    m_storage.destroy(m_pipelines.genSpMap.pipeline,   getDevice() );
    m_storage.destroy(m_pipelines.genIRMap.pipeline,   getDevice() );
    m_storage.destroy(m_pipelines.genBRDF.pipeline,   getDevice() );

    m_storage.destroyAll(getDevice());
}

std::unique_ptr<ScopedFence> PBRTextureGenerator::convertToCube(gvk::ObjectID_t<Image> dstCube, gvk::ObjectID_t<Image> srcRect)
{
    auto kEnvMapSize = get(dstCube).imageCreateInfo.extent.height;
    assert(kEnvMapSize >= 32 );
    VkDescriptorSet set = m_pipelines.rectToCube.set;
    {
        auto computeSampler = m_pipelines.rectToCube.sampler;

        vkb::DescriptorSetWriter W;

        W.updateSet(set, 0, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER)
                .setImageInfo( {{ computeSampler, getView(srcRect), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL}});

        W.updateSet(set, 1, VK_DESCRIPTOR_TYPE_STORAGE_IMAGE)
                .setImageInfo( {{ computeSampler, getView(dstCube), VK_IMAGE_LAYOUT_GENERAL}} );

        W.update( getDevice() );

    }

    return registry->beginCommandBuffer([&](auto commandBuffer)
    {
        registry->image_TransitionLayout(commandBuffer, dstCube, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_GENERAL);

        vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_COMPUTE, m_pipelines.rectToCube.pipeline);

        auto layout = std::get<vk::PipelineLayout>(m_storage.getCreateInfo<vkb::ComputePipelineCreateInfo2>(m_pipelines.rectToCube.pipeline).layout);

        vkCmdBindDescriptorSets(commandBuffer, VK_PIPELINE_BIND_POINT_COMPUTE, layout, 0, 1, &set, 0, nullptr);
        vkCmdDispatch(commandBuffer, kEnvMapSize/32, kEnvMapSize/32, 6);

        registry->image_TransitionLayout(commandBuffer, dstCube, VK_IMAGE_LAYOUT_GENERAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
    });
}

void PBRTextureGenerator::generateSpMapInPlace(gvk::ObjectID_t<Image> cube)
{
    auto kEnvMapSize      = get(cube).imageCreateInfo.extent.height;
    uint32_t totalMips    = get(cube).imageCreateInfo.mipLevels;
    auto kEnvMapLevels    = get(cube).imageCreateInfo.mipLevels;
    auto numMipTailLevels = kEnvMapLevels-1;

    assert(kEnvMapSize >= 32 );

    auto computeSampler = m_pipelines.rectToCube.sampler;
    VkDescriptorSet set = m_pipelines.genSpMap.set;

    vkb::DescriptorSetWriter W;


    // 1. Generate the view for mipmap 0
    // this will be the source for the compute
    // shader, which we will read from to generate
    // the other mip maps.
    auto mip0View = _generateMipMapView(cube, 0 );

    W.updateSet(set, 0, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER)
            .setImageInfo( {{ computeSampler, mip0View, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL}});

    // 2. Generate views for each of the other mip maps
    // in the cube image. we need to generate 16
    // mipmaps because this is how many the shader requries
    std::vector<VkDescriptorImageInfo> mipInfos;
    for(uint32_t m=1;m<16;m++)
    {
        auto & I = mipInfos.emplace_back();
        I.imageView   = _generateMipMapView(cube, std::min(totalMips-1, m) );
        I.sampler     = computeSampler;
        I.imageLayout = VK_IMAGE_LAYOUT_GENERAL;//vk::ImageLayout::eGeneral;
    }
    W.updateSet(set, 1, VK_DESCRIPTOR_TYPE_STORAGE_IMAGE)
            .setImageInfo(mipInfos);

    // update the descriptor sets for the
    // compute shader
    W.update( getDevice() );

    registry->beginCommandBuffer([&](auto commandBuffer)
    {
        // 3. Convert mip levels 1 - N into LAYOUT_GENERAL
        //    mip 0 should already be in read only optimal
        registry->image_TransitionLayout(commandBuffer, cube, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, VK_IMAGE_LAYOUT_GENERAL, 0, 6, 1, totalMips-1);

        // 4. bind the pipeline and the descriptor sets
        vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_COMPUTE, m_pipelines.genSpMap.pipeline);
        vkCmdBindDescriptorSets(commandBuffer,
                                VK_PIPELINE_BIND_POINT_COMPUTE,
                                getLayout(m_pipelines.genSpMap.pipeline), 0,
                                1,
                                &set,
                                0,
                                nullptr);

        const float deltaRoughness = 1.0f / std::max(float(numMipTailLevels), 1.0f);

        // 5. for each of the mip map levels from 1 - kEnvMapLevels
        //    execute the compute shader
        for(uint32_t level=1, size=kEnvMapSize/2; level<kEnvMapLevels; ++level, size/=2)
        {
            const uint32_t numGroups = std::max<uint32_t>(1, size/32);

            struct SpecularFilterPushConstants
            {
                uint32_t level;
                float    roughness;
            };
            const SpecularFilterPushConstants pushConstants = { level-1, static_cast<float>(level) * deltaRoughness };

            vkCmdPushConstants(commandBuffer, getLayout(m_pipelines.genSpMap.pipeline), VK_SHADER_STAGE_COMPUTE_BIT, 0, sizeof(SpecularFilterPushConstants), &pushConstants);
            vkCmdDispatch(commandBuffer, numGroups, numGroups, 6);
        }

        registry->image_TransitionLayout(commandBuffer, cube, VK_IMAGE_LAYOUT_GENERAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, 0, 6, 1, totalMips-1);
    });

    for(auto v : mipInfos)
    {
        vkDestroyImageView( getDevice(), v.imageView, nullptr);
    }
    vkDestroyImageView( getDevice(), mip0View, nullptr);
}


std::unique_ptr<ScopedFence> PBRTextureGenerator::generateIRMap(gvk::ObjectID_t<Image> dstCube, gvk::ObjectID_t<Image> srcCube)
{
    spdlog::info("BEGIN generating IR Map");
    auto pipeline = m_pipelines.genIRMap.pipeline;
    auto computePipelineLayout = getLayout(pipeline);
    VkDescriptorSet computeDescriptorSet = m_pipelines.genIRMap.set;
    uint32_t kIrradianceMapSize = get(dstCube).imageCreateInfo.extent.height;
    VkSampler computeSampler = m_pipelines.rectToCube.sampler;

    {
        vkb::DescriptorSetWriter W;

        W.updateSet(computeDescriptorSet, 0, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER)
                .setImageInfo( {{ computeSampler, getView(srcCube), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL}});

        W.updateSet(computeDescriptorSet, 1, VK_DESCRIPTOR_TYPE_STORAGE_IMAGE)
                .setImageInfo( {{ computeSampler, getView(dstCube), VK_IMAGE_LAYOUT_GENERAL}} );

        W.update( getDevice() );

    }

    return registry->beginCommandBuffer([&, this](auto commandBuffer)
    {
        registry->image_TransitionLayout(commandBuffer, dstCube, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_GENERAL);

        vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_COMPUTE, pipeline);
        vkCmdBindDescriptorSets(commandBuffer, VK_PIPELINE_BIND_POINT_COMPUTE, computePipelineLayout, 0, 1, &computeDescriptorSet, 0, nullptr);
        vkCmdDispatch(commandBuffer, kIrradianceMapSize/32, kIrradianceMapSize/32, 6);

        registry->image_TransitionLayout(commandBuffer, dstCube, VK_IMAGE_LAYOUT_GENERAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
    });
    spdlog::info("FINISHED generating IR Map");
}

gvk::ObjectID_t<Image> PBRTextureGenerator::generateBRDF(uint32_t width)
{
    auto kBRDF_LUT_Size = width;

    auto pipeline = m_pipelines.genBRDF.pipeline;
    auto computePipelineLayout = getLayout(pipeline);
    VkDescriptorSet computeDescriptorSet = m_pipelines.genBRDF.set;
    VkSampler computeSampler = m_pipelines.rectToCube.sampler;

    gvk::ObjectID_t<Image> dstCube = registry->image2D_Create(width,width, VK_FORMAT_R16G16_SFLOAT, 1, 1, VK_IMAGE_USAGE_STORAGE_BIT);

    {
        vkb::DescriptorSetWriter W;

        W.updateSet(computeDescriptorSet, 0, VK_DESCRIPTOR_TYPE_STORAGE_IMAGE)
                .setImageInfo( {{ computeSampler, getView(dstCube), VK_IMAGE_LAYOUT_GENERAL}} );

        W.update( getDevice() );

    }


    registry->beginCommandBuffer([&, this](auto commandBuffer)
    {
        registry->image_TransitionLayout(commandBuffer, dstCube, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_GENERAL);

        vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_COMPUTE, pipeline);
        vkCmdBindDescriptorSets(commandBuffer, VK_PIPELINE_BIND_POINT_COMPUTE, computePipelineLayout, 0, 1, &computeDescriptorSet, 0, nullptr);
        vkCmdDispatch(commandBuffer, kBRDF_LUT_Size/32, kBRDF_LUT_Size/32, 6);

        registry->image_TransitionLayout(commandBuffer, dstCube, VK_IMAGE_LAYOUT_GENERAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
    });

    return dstCube;
}

VkImageView PBRTextureGenerator::_generateMipMapView(gvk::ObjectID_t<Image> dstCube, uint32_t mipLevel)
{
    auto & dstImg = get(dstCube);
    {
        VkImageViewCreateInfo ci{};
        ci.sType    = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
        ci.image    = dstImg._image;
        ci.viewType = VK_IMAGE_VIEW_TYPE_CUBE;
        ci.format   = dstImg.imageCreateInfo.format;
        ci.components = { VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_G, VK_COMPONENT_SWIZZLE_B, VK_COMPONENT_SWIZZLE_A };

        ci.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        ci.subresourceRange.baseArrayLayer = 0;
        ci.subresourceRange.layerCount = 6;

        ci.subresourceRange.baseMipLevel = mipLevel;
        ci.subresourceRange.levelCount = 1;

        switch(ci.format)
        {
        case VK_FORMAT_D16_UNORM:
        case VK_FORMAT_D32_SFLOAT:
        case VK_FORMAT_D16_UNORM_S8_UINT:
        case VK_FORMAT_D24_UNORM_S8_UINT:
        case VK_FORMAT_D32_SFLOAT_S8_UINT:
            ci.subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;// vk::ImageAspectFlagBits::eDepth;
            break;
        default:
            ci.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT; //vk::ImageAspectFlagBits::eColor;
            break;
        }

        VkImageView view;
        VK_CHECK_RESULT( vkCreateImageView(getDevice(), &ci, nullptr, &view) );
        return view;
    }
}

VkPipelineLayout PBRTextureGenerator::getLayout(VkPipeline p) const
{
    auto layout = std::get<vk::PipelineLayout>(m_storage.getCreateInfo<vkb::ComputePipelineCreateInfo2>(p).layout);
    return layout;
}

VkImageView PBRTextureGenerator::getView(gvk::ObjectID_t<Image> id) const
{
    return get(id)._view;
}

VkDevice PBRTextureGenerator::getDevice() const
{
    return device;
}

void PBRTextureGenerator::_initDSetAllocator()
{
    vkb::DescriptorPoolCreateInfo2 info;

    info.setMaxSets(1)
            .setPoolSize(vk::DescriptorType::eCombinedImageSampler, 10)
            .setPoolSize(vk::DescriptorType::eUniformBuffer       , 10)
            .setPoolSize(vk::DescriptorType::eStorageBuffer       , 10)
            .setPoolSize(vk::DescriptorType::eStorageBufferDynamic, 10)
            .setPoolSize(vk::DescriptorType::eUniformBufferDynamic, 10)
            .setPoolSize(vk::DescriptorType::eStorageImage, 45)
            .setFlags(   vk::DescriptorPoolCreateFlagBits::eFreeDescriptorSet
                         | vk::DescriptorPoolCreateFlagBits::eUpdateAfterBind);

    m_pipelines.dSetAllocator.init( &m_storage, getDevice(), info);
}

void PBRTextureGenerator::_initComputePipelines()
{
    {
        vkb::SamplerCreateInfo2 ci;

        ci.minFilter   = vk::Filter::eLinear;
        ci.magFilter   = vk::Filter::eLinear;
        ci.borderColor = vk::BorderColor::eFloatTransparentBlack;

        m_pipelines.rectToCube.sampler = ci.create( m_storage, getDevice());
    }

    {
        auto f = resourceLocator->locate("shaders/eqirect2cube.comp");
        auto shaderCode = gnl::GLSLCompiler::compileFromFile(f);
        assert(shaderCode.size() > 0);

        vkb::ComputePipelineCreateInfo2 ci;
        ci.stage.code = shaderCode;
        ci.stage.name = "main";
        ci.layout = _generatePipelineLayoutCreateInfo( {{shaderCode, VK_SHADER_STAGE_COMPUTE_BIT} });

        m_pipelines.rectToCube.pipeline = ci.create(m_storage, getDevice());

        {
            std::vector< vkb::DescriptorSetAllocator::DescriptorSetBinding > allocInfo = {
                { 0, vk::DescriptorType::eCombinedImageSampler, 1, vk::ShaderStageFlagBits::eCompute, {m_pipelines.rectToCube.sampler}},
                { 1, vk::DescriptorType::eStorageImage, 1, vk::ShaderStageFlagBits::eCompute, {}}
            };
            m_pipelines.rectToCube.set = m_pipelines.dSetAllocator.allocateDescriptorSet(allocInfo);
        }
    }



    {
        auto f = resourceLocator->locate("shaders/genSpMap.comp");
        auto shaderCode = gnl::GLSLCompiler::compileFromFile(f);
        assert(shaderCode.size() > 0);

        vkb::ComputePipelineCreateInfo2 ci;
        ci.stage.code = shaderCode;
        ci.stage.name = "main";

        auto layout = _generatePipelineLayoutCreateInfo( {{shaderCode, VK_SHADER_STAGE_COMPUTE_BIT} });
        layout.addPushConstantRange(vk::ShaderStageFlagBits::eCompute, 0, 128);
        ci.layout = layout;
        m_pipelines.genSpMap.pipeline = ci.create(m_storage, getDevice());

        {

            std::vector< vkb::DescriptorSetAllocator::DescriptorSetBinding > allocInfo = {
                { 0, vk::DescriptorType::eCombinedImageSampler, 1, vk::ShaderStageFlagBits::eCompute, {m_pipelines.rectToCube.sampler}},
                { 1, vk::DescriptorType::eStorageImage, 15, vk::ShaderStageFlagBits::eCompute, {}}
            };
            m_pipelines.genSpMap.set = m_pipelines.dSetAllocator.allocateDescriptorSet(allocInfo);
        }
    }

    {
        auto f = resourceLocator->locate("shaders/genIRMap.comp");
        auto shaderCode = gnl::GLSLCompiler::compileFromFile(f);
        assert(shaderCode.size() > 0);

        vkb::ComputePipelineCreateInfo2 ci;
        ci.stage.code = shaderCode;
        ci.stage.name = "main";

        auto layout = _generatePipelineLayoutCreateInfo( {{shaderCode, VK_SHADER_STAGE_COMPUTE_BIT} });
        layout.addPushConstantRange(vk::ShaderStageFlagBits::eCompute, 0, 128);
        ci.layout = layout;
        m_pipelines.genIRMap.pipeline = ci.create(m_storage, getDevice());

        {

            std::vector< vkb::DescriptorSetAllocator::DescriptorSetBinding > allocInfo = {
                { 0, vk::DescriptorType::eCombinedImageSampler, 1, vk::ShaderStageFlagBits::eCompute, {m_pipelines.rectToCube.sampler}},
                { 1, vk::DescriptorType::eStorageImage, 1, vk::ShaderStageFlagBits::eCompute, {}}
            };
            m_pipelines.genIRMap.set = m_pipelines.dSetAllocator.allocateDescriptorSet(allocInfo);
        }
    }

    {
        auto f = resourceLocator->locate("shaders/genBRDF.comp");
        auto shaderCode = gnl::GLSLCompiler::compileFromFile(f);
        assert(shaderCode.size() > 0);

        vkb::ComputePipelineCreateInfo2 ci;
        ci.stage.code = shaderCode;
        ci.stage.name = "main";

        auto layout = _generatePipelineLayoutCreateInfo( {{shaderCode, VK_SHADER_STAGE_COMPUTE_BIT} });
        layout.addPushConstantRange(vk::ShaderStageFlagBits::eCompute, 0, 128);
        ci.layout = layout;
        m_pipelines.genBRDF.pipeline = ci.create(m_storage, getDevice());

        {

            std::vector< vkb::DescriptorSetAllocator::DescriptorSetBinding > allocInfo = {
                { 0, vk::DescriptorType::eStorageImage, 1, vk::ShaderStageFlagBits::eCompute, {}}
            };
            m_pipelines.genBRDF.set = m_pipelines.dSetAllocator.allocateDescriptorSet(allocInfo);
        }
    }
}

vkb::PipelineLayoutCreateInfo2 PBRTextureGenerator::_generatePipelineLayoutCreateInfo(const std::vector<std::pair<std::vector<uint32_t> &, VkShaderStageFlagBits> > &e)
{
    vkb::SPIRV_DescriptorSetLayoutGenerator layoutGenerator;
    for(auto & x : e)
    {
        layoutGenerator.addSPIRVCode( x.first, x.second );
    }
    vkb::PipelineLayoutCreateInfo2 layout;
    layoutGenerator.generate([&](auto & D)
    {
        for(auto & d : D)
        {
            // std::cout << "Set: " << d.first << std::endl;
            auto & Set = layout.newDescriptorSet();
            for(auto & e1 : d.second)
            {
                //std::cout << "binding: " << e1.binding << "   count: " << e.descriptorCount << " type: " << to_string(static_cast<vk::DescriptorType>(e1.descriptorType)) << std::endl;
                Set.addDescriptor(e1.binding, static_cast<vk::DescriptorType>(e1.descriptorType), e1.descriptorCount, static_cast<vk::ShaderStageFlags>(e1.stageFlags));
            }
        }
    });
    return layout;
}

}

