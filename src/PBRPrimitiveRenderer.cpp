#include <vulkan/vulkan.h>
#include <vector>
#include <cstdint>
#include <exception>
#include <stdexcept>
#include <cassert>

#include <vkb/utils/DynamicPipeline.h>
#include <vkb/utils/DescriptorSetAllocator.h>
#include <vkb/utils/DescriptorUpdater.h>
#include <vkb/utils/multistoragebuffer.h>
#include <vkb/utils/TextureArrayDescriptorSet.h>
#include <vkb/utils/SPIRV_DescriptorSetLayoutGenerator.h>
#include <vkb/detail/ComputePipelineCreateInfo2.h>
#include <vkb/vkb.h>

#include <gul/math/Transform.h>
#include <gul/MeshPrimitive.h>
#include <gul/ResourceLocator.h>

#include <gvk/Objects/Primtive.h>
#include <gvk/Objects/PBRMaterial.h>
#include <gvk/L1.h>

#include <glm/glm.hpp>
#include <spdlog/spdlog.h>

#include <gvk/Renderers/PBRPrimitiveRenderer.h>
#include <gvk/Renderers/PBRGenerator.h>

namespace gvk
{


void PBRPrimitiveRenderer::initResources(PBRPrimitiveRenderer::InitInfo I)
{
    m_initInfo = I;
    auto & L = *I.registry;
    m_shaderWriteBuffer = L.buffer_Create( MAX_DEPTH_BUCKETS*sizeof(uint32_t)  , VK_BUFFER_USAGE_STORAGE_BUFFER_BIT, VMA_MEMORY_USAGE_GPU_TO_CPU);
    m_transformBuffer   = L.buffer_Create( MAX_TRANSFORMS * sizeof(glm::mat4)  , VK_BUFFER_USAGE_STORAGE_BUFFER_BIT, VMA_MEMORY_USAGE_CPU_TO_GPU);
    m_PBRMaterialBuffer = L.buffer_Create(  MAX_MATERIALS * sizeof(KHRMaterial_shader), VK_BUFFER_USAGE_STORAGE_BUFFER_BIT, VMA_MEMORY_USAGE_CPU_TO_GPU);
    m_GlobalUniform     = L.buffer_Create(sizeof(GlobalUniform), VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VMA_MEMORY_USAGE_CPU_TO_GPU);

    {
        glm::mat4 * mapped = static_cast<glm::mat4*>(L.bufferMapData(m_transformBuffer));
        m_transformStorage.init(mapped, L.get(m_transformBuffer)._requestedSize);
    }
    {
        KHRMaterial_shader * mapped = static_cast<KHRMaterial_shader*>(L.bufferMapData(m_PBRMaterialBuffer));
        m_materialStorage.init(mapped, L.get(m_PBRMaterialBuffer)._requestedSize);
    }

    initDSetAllocator();
    initPipeline();

    std::vector<vkb::DescriptorSetAllocator::DescriptorSetBinding> bindings = {
        {0, vk::DescriptorType::eUniformBuffer, 1, vk::ShaderStageFlagBits::eVertex | vk::ShaderStageFlagBits::eFragment, {}},
        {1, vk::DescriptorType::eStorageBuffer, 1, vk::ShaderStageFlagBits::eVertex | vk::ShaderStageFlagBits::eFragment, {}},
        {2, vk::DescriptorType::eStorageBuffer, 1, vk::ShaderStageFlagBits::eVertex | vk::ShaderStageFlagBits::eFragment, {}},
        {3, vk::DescriptorType::eStorageBuffer, 1, vk::ShaderStageFlagBits::eFragment | vk::ShaderStageFlagBits::eFragment, {}}
    };

    m_storageBufferDescriptorSet = m_pipelines.dSetAllocator.allocateDescriptorSet( bindings);


    vkb::DescriptorSetUpdater2 updater;

    // update the uniform and storage descriptors
    {
        vk::DescriptorBufferInfo binfo;
        binfo.offset = 0;

        binfo.buffer = L.get(m_GlobalUniform)._buffer;
        updater.updateBufferDescriptor(m_storageBufferDescriptorSet, 0, 0, vk::DescriptorType::eUniformBuffer, binfo);

        binfo.buffer = L.get(m_transformBuffer)._buffer;
        updater.updateBufferDescriptor(m_storageBufferDescriptorSet, 1, 0, vk::DescriptorType::eStorageBuffer, binfo);

        binfo.buffer = L.get(m_PBRMaterialBuffer)._buffer;
        updater.updateBufferDescriptor(m_storageBufferDescriptorSet, 2, 0, vk::DescriptorType::eStorageBuffer, binfo);

        binfo.buffer = L.get(m_shaderWriteBuffer)._buffer;
        updater.updateBufferDescriptor(m_storageBufferDescriptorSet, 3, 0, vk::DescriptorType::eStorageBuffer, binfo);

        updater.update(m_initInfo.device);
    }


    vkb::TextureArrayDescriptorSetChainCreateInfo ci1;
    ci1.chainSize = 5;

    {

        gul::Image img;
        img.resize(64,64,4);
        img.r = 255;
        img.g = 0;
        img.b = 0;
        img.a = 255;

        auto id = L.image2D_Create(64,64, VK_FORMAT_R8G8B8A8_UNORM, 1, 0);

        L.imageCopyData(id, img.data(), img.size());

        // Create a texture array descriptor set chain
        // which is a chain of texture arrays
        auto & info = ci1.bindings.emplace_back();
        info.descriptorSetBinding = 0;
        info.defaultImageLayout = vk::ImageLayout::eShaderReadOnlyOptimal;
        info.defaultImageView   = L.get(id)._view;
        info.defaultSampler     = L.get(id).sampler.linear;
        info.textureCount       = 32;
    }





    {
        uint32_t cubeSize=32;

        gul::Image img;
        img.resize(cubeSize,cubeSize,4);
        img.r = 255;
        img.g = 0;
        img.b = 0;
        img.a = 255;

        auto id = L.imageCube_Create(32,VK_FORMAT_R8G8B8A8_UNORM, 1, 0);

        std::vector<uint8_t> rawData( img.size() * 6);

        // total byte size of the cube
        auto byteSize  = img.size() * 6;

        auto stagingBuffer = m_initInfo.registry->buffer_Create(byteSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VMA_MEMORY_USAGE_CPU_TO_GPU);
        auto mapped        = m_initInfo.registry->bufferMapData(stagingBuffer);
        std::memcpy(mapped, rawData.data(), byteSize);

        m_initInfo.registry->bufferFlushData(stagingBuffer,0, byteSize);
        m_initInfo.registry->bufferUnmapData(stagingBuffer);

        m_initInfo.registry->beginCommandBuffer([&](auto commandBuffer)
        {
            m_initInfo.registry->image_TransitionLayout(commandBuffer,id, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);
            m_initInfo.registry->imageCopyIntoLayer(commandBuffer, id, 0, stagingBuffer, 0, {cubeSize,cubeSize});
            m_initInfo.registry->imageCopyIntoLayer(commandBuffer, id, 1, stagingBuffer, 0, {cubeSize,cubeSize});
            m_initInfo.registry->imageCopyIntoLayer(commandBuffer, id, 2, stagingBuffer, 0, {cubeSize,cubeSize});
            m_initInfo.registry->imageCopyIntoLayer(commandBuffer, id, 3, stagingBuffer, 0, {cubeSize,cubeSize});
            m_initInfo.registry->imageCopyIntoLayer(commandBuffer, id, 4, stagingBuffer, 0, {cubeSize,cubeSize});
            m_initInfo.registry->imageCopyIntoLayer(commandBuffer, id, 5, stagingBuffer, 0, {cubeSize,cubeSize});
            m_initInfo.registry->image_TransitionLayout(commandBuffer,id, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
        });

        // Create a texture array descriptor set chain
        // which is a chain of texture arrays
        auto & info = ci1.bindings.emplace_back();
        info.descriptorSetBinding = 1;
        info.defaultImageLayout = vk::ImageLayout::eShaderReadOnlyOptimal;
        info.defaultImageView   = L.get(id)._view;
        info.defaultSampler     = L.get(id).sampler.linear;
        info.textureCount       = 32;
    }



    m_imageDescriptorChain.create(m_initInfo.device, m_storage, ci1);

    {
        m_BRDF   = m_initInfo.textureGenerator->generateBRDF(256);
        m_IrrMap = m_initInfo.registry->imageCube_Create(m_IrradianceMapSize, VK_FORMAT_R8G8B8A8_UNORM, 0, VK_IMAGE_USAGE_STORAGE_BIT);
        m_EnvMap = m_initInfo.registry->imageCube_Create(m_RadianceMapSize  , VK_FORMAT_R8G8B8A8_UNORM, 0, VK_IMAGE_USAGE_STORAGE_BIT);
    }
}

void PBRPrimitiveRenderer::releaseResources()
{
    auto & L = *m_initInfo.registry;

    m_imageDescriptorChain.destroy(m_initInfo.device, m_storage);

    m_pipelines.dSetAllocator.destroy();

    L.buffer_Destroy(m_transformBuffer);
    L.buffer_Destroy(m_PBRMaterialBuffer);
    L.buffer_Destroy(m_GlobalUniform);

    L.image_Destroy(m_BRDF);
    L.image_Destroy(m_IrrMap);
    L.image_Destroy(m_EnvMap);
    //m_storage.destroy(vk::DescriptorPool(m_descriptorPool), m_initInfo.device);
    m_storage.destroyAll(m_initInfo.device);

    m_KHR_Pipeline.destroy();
}

void PBRPrimitiveRenderer::generateNewEnvironment(gvk::ObjectID_t<Image> rectImage)
{
    auto & m_generator = *m_initInfo.textureGenerator;

    // convert the rect image to a cube and copy it into the
    // first mipmap level
    auto t0 = std::chrono::system_clock::now();
    m_generator.convertToCube(m_EnvMap   , rectImage);
    auto t1 = std::chrono::system_clock::now();
    spdlog::info("Converting to cube: {} seconds", std::chrono::duration<double>(t1-t0).count());

    m_generator.generateSpMapInPlace(m_EnvMap);
    auto t2 = std::chrono::system_clock::now();
    spdlog::info("Generating SP Map: {} seconds", std::chrono::duration<double>(t2-t1).count());

    m_generator.generateIRMap(m_IrrMap, m_EnvMap);
    auto t3 = std::chrono::system_clock::now();
    spdlog::info("Generating IRR Map: {} seconds", std::chrono::duration<double>(t3-t2).count());
}

int32_t PBRPrimitiveRenderer::stageTexture(gvk::ObjectID_t<Image> img, VkSampler sampler)
{
    auto & I = m_initInfo.registry->get(img);
    switch(I._viewType)
    {
    case VK_IMAGE_VIEW_TYPE_2D:
        return m_imageDescriptorChain.insertTexture( I._view, 0, sampler==VK_NULL_HANDLE ? I.sampler.linear : sampler); break;
    case VK_IMAGE_VIEW_TYPE_CUBE:
        return m_imageDescriptorChain.insertTexture( I._view, 1, sampler==VK_NULL_HANDLE ? I.sampler.linear : sampler); break;
    default:
        return -1;
    }

}

int32_t PBRPrimitiveRenderer::unstageTexture(gvk::ObjectID_t<Image> img)
{
    auto & I = m_initInfo.registry->get(img);
    switch(I._viewType)
    {
    case VK_IMAGE_VIEW_TYPE_2D:
        return m_imageDescriptorChain.removeTexture( m_initInfo.registry->get(img)._view, 0); break;
    case VK_IMAGE_VIEW_TYPE_CUBE:
        return m_imageDescriptorChain.removeTexture( m_initInfo.registry->get(img)._view, 1); break;
    default:
        return -1;
    }
}

void PBRPrimitiveRenderer::beginRender(PBRPrimitiveRenderer::BeginInfo B)
{
    m_info = B;
    m_lastPrimitive = {};


    {
        auto & L = *m_initInfo.registry;

        m_GlobalUniformData.BRDF_INDEX   = stageTexture(m_BRDF  , L.getSampler(VK_FILTER_LINEAR, VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE));
        m_GlobalUniformData.IRRMAP_INDEX = stageTexture(m_IrrMap, L.getSampler(VK_FILTER_LINEAR, VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE));
        m_GlobalUniformData.RADMAP_INDEX = stageTexture(m_EnvMap, L.getSampler(VK_FILTER_LINEAR, VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE));
        m_GlobalUniformData.MOUSE_POS    = B.mousePos;
        m_GlobalUniformData.MOUSE_HOVER_UNIQUE_ID = getMouseHoverID();
        m_GlobalUniformData.FRAME_NUMBER++;

        auto now = std::chrono::system_clock::now();
        auto dt = std::chrono::duration<double>(now - m_timePoint).count();
        m_timePoint = now;
        m_GlobalUniformData.FRAME_TIME_DELTA = static_cast<float>(dt);

        m_GlobalUniformData.TIMER_0 += static_cast<float>(dt);
        m_GlobalUniformData.TIMER_1 += static_cast<float>(dt);
        m_GlobalUniformData.TIMER_2 += static_cast<float>(dt);
        m_GlobalUniformData.TIMER_3 += static_cast<float>(dt);

        auto * mapped = static_cast<GlobalUniform*>(L.bufferMapData(m_GlobalUniform));

        std::memcpy(mapped, &m_GlobalUniformData, sizeof(m_GlobalUniformData));
    }

    m_KHR_Pipeline.setRenderPass(B.renderPass);
    m_piplineBind = m_KHR_Pipeline.bind(B.commandBuffer);

    vkCmdBindDescriptorSets(B.commandBuffer,
                            VK_PIPELINE_BIND_POINT_GRAPHICS,
                            std::get<1>(m_piplineBind),
                            0,
                            1,
                            &m_storageBufferDescriptorSet,
                            0, nullptr);

    m_imageDescriptorChain.nextArray();
    vkb::DescriptorSetUpdater updater;
    auto c = m_imageDescriptorChain.update(updater);
    updater.update(m_initInfo.device);

    if(c)
        spdlog::info("Updated {} textures", c);
#if 1
    VkDescriptorSet set = m_imageDescriptorChain.getCurrentDescriptorSet();
    vkCmdBindDescriptorSets(B.commandBuffer,
                            VK_PIPELINE_BIND_POINT_GRAPHICS,
                            std::get<1>(m_piplineBind),
                            1,
                            1,
                            &set,
                            0, nullptr);
#endif
}

void PBRPrimitiveRenderer::endRender()
{
    m_lastPrimitive = {};
    m_info          = {};
    auto * v = m_initInfo.registry->bufferMapData(m_shaderWriteBuffer);

    auto u = static_cast<uint32_t*>(v);

    for(size_t i=0;i<MAX_DEPTH_BUCKETS;i++)
    {
        if( u[i] != 0)
        {
            m_uniqueIDMouseHover = u[i];
            u[i] = 0;
            break;
        }
    }
    //std::cout << std::endl;
    std::memset(u, 0, sizeof(uint32_t)*MAX_DEPTH_BUCKETS);
}

uint32_t PBRPrimitiveRenderer::getMouseHoverID() const
{
    return m_uniqueIDMouseHover;
}

void PBRPrimitiveRenderer::bindPrimitive(gvk::ObjectID_t<Primitive> pID)
{
    if( pID == m_lastPrimitive)
        return;

    auto c = m_info.commandBuffer;
    auto & Pr = m_initInfo.registry->get(pID);
    auto & B  = m_initInfo.registry->get(Pr.buffer);

    std::array<VkBuffer,10> buffs;
    buffs.fill(B._buffer);

    uint32_t bindingCount = static_cast<uint32_t>(Pr.vertexOffsets.size());

    vkCmdBindVertexBuffers(c, 0,bindingCount, buffs.data(), Pr.vertexOffsets.data());

    m_indexCount  = 0;
    m_vertexCount = Pr.vertexCount;

    if( Pr.indexCount)
    {
        vkCmdBindIndexBuffer(c, B._buffer, Pr.indexOffset, Pr.indexType);

        m_indexCount = Pr.indexCount;
    }

    vkCmdPushConstants(c,
                       std::get<1>(m_piplineBind),
                       VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT,
                       offsetof(PushConstants, attributeFlags),
                       sizeof(Pr.vertexAttributeMask),
                       &Pr.vertexAttributeMask);
    m_lastPrimitive = pID;

}

void PBRPrimitiveRenderer::draw(uint32_t instanceCount, uint32_t firstInstance)
{
    VkCommandBuffer cmd = m_info.commandBuffer;
    if( m_indexCount)
        vkCmdDrawIndexed(cmd, m_indexCount, instanceCount, 0, 0, firstInstance);
    else
        vkCmdDraw( cmd, m_vertexCount, instanceCount, 0, 0);
}

void PBRPrimitiveRenderer::setProjMatrix(const glm::mat4 &P)
{
#if 1
    auto index = m_transformStorage.push(P);

    vkCmdPushConstants(m_info.commandBuffer,
                       std::get<1>(m_piplineBind),
                       VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT,
                       offsetof(PushConstants, projMatrixIndex),
                       sizeof(index),
                       &index);
#else
    vkCmdPushConstants(m_info.commandBuffer, std::get<1>(m_piplineBind), VK_SHADER_STAGE_VERTEX_BIT, 0,sizeof(glm::mat4), &P);
#endif
}

void PBRPrimitiveRenderer::setViewMatrix(const glm::mat4 &V)
{

    std::array<glm::mat4, 2> matrices = {V, glm::inverse(V)};

    auto index = m_transformStorage.push(matrices.data(), 2);

    vkCmdPushConstants(m_info.commandBuffer,
                       std::get<1>(m_piplineBind),
                       VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT,
                       offsetof(PushConstants, viewMatrixIndex),
                       sizeof(index),
                       &index);
}

void PBRPrimitiveRenderer::setProjViewMatrix(const glm::mat4 &PV)
{
#if 1
    auto index = m_transformStorage.push(PV);

    vkCmdPushConstants(m_info.commandBuffer,
                       std::get<1>(m_piplineBind),
                       VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT,
                       offsetof(PushConstants, projViewMatrixIndex),
                       sizeof(index),
                       &index);
#else
    vkCmdPushConstants(m_info.commandBuffer, std::get<1>(m_piplineBind), VK_SHADER_STAGE_VERTEX_BIT, 0,sizeof(glm::mat4), &PV);
#endif

}

int32_t PBRPrimitiveRenderer::setModelMatrix(const glm::mat4 &M)
{
#if 1
    auto index = m_transformStorage.push(M);

    vkCmdPushConstants(m_info.commandBuffer,
                       std::get<1>(m_piplineBind),
                       VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT,
                       offsetof(PushConstants, modelMatrixIndex),
                       sizeof(index),
                       &index);
    return index;
#else
    vkCmdPushConstants(m_info.commandBuffer, std::get<1>(m_piplineBind), VK_SHADER_STAGE_VERTEX_BIT, sizeof(glm::mat4),sizeof(glm::mat4), &M);
#endif
}

int32_t PBRPrimitiveRenderer::setModelMatrices(const glm::mat4 *M, size_t N)
{
#if 1
    auto index = m_transformStorage.push(M, N);

    vkCmdPushConstants(m_info.commandBuffer,
                       std::get<1>(m_piplineBind),
                       VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT,
                       offsetof(PushConstants, modelMatrixIndex),
                       sizeof(index),
                       &index);
    return index;
#else
    vkCmdPushConstants(m_info.commandBuffer, std::get<1>(m_piplineBind), VK_SHADER_STAGE_VERTEX_BIT, sizeof(glm::mat4),sizeof(glm::mat4), &M);
#endif
}

KHRMaterial_shader PBRPrimitiveRenderer::generateMaterialUniform(gvk::ObjectID_t<PBRMaterial> mat)
{
    auto & M = m_initInfo.registry->get(mat);
    KHRMaterial_shader Ms;
    Ms.baseColorFactor    = M.baseColorFactor    ;
    Ms.emissiveFactor     = M.emissiveFactor     ;
    Ms.metallicFactor     = M.metallicFactor     ;
    Ms.roughnessFactor    = M.roughnessFactor    ;
    Ms.alphaCutoff        = M.alphaCutoff        ;
    Ms.vertexNormalOffset = M.vertexNormalOffset ;

    Ms.baseColorTextureIndex         = valid(M.textures.baseColor        ) ? stageTexture(M.textures.baseColor)         : -1;
    Ms.normalTextureIndex            = valid(M.textures.normal           ) ? stageTexture(M.textures.normal)            : -1;
    Ms.metallicRoughnessTextureIndex = valid(M.textures.metallicRoughness) ? stageTexture(M.textures.metallicRoughness) : -1;
    Ms.emissiveTextureIndex          = valid(M.textures.emissive)          ? stageTexture(M.textures.emissive)          : -1;
    return Ms;
}

void PBRPrimitiveRenderer::setMaterial(const KHRMaterial_shader &Ms, uint32_t materialNumber)
{
    if(materialNumber>2)
        materialNumber=2;

    auto index = m_materialStorage.push(Ms);

    vkCmdPushConstants(m_info.commandBuffer,
                       std::get<1>(m_piplineBind),
                       VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT,
                       static_cast<uint32_t>(offsetof(PushConstants, materialIndex0) + sizeof(uint32_t)*materialNumber),
                       sizeof(index),
                       &index);
}

void PBRPrimitiveRenderer::setMaterial(gvk::ObjectID_t<PBRMaterial> mat, uint32_t materialNumber)
{
    KHRMaterial_shader Ms = generateMaterialUniform(mat);
    setMaterial(Ms, materialNumber);
}

void PBRPrimitiveRenderer::resetTimer(uint32_t timerIndex, float value)
{
    timerIndex = std::min(timerIndex,3u);
    (&m_GlobalUniformData.TIMER_0)[timerIndex] = value;
}

void PBRPrimitiveRenderer::setEntityID(uint32_t id)
{
    vkCmdPushConstants(m_info.commandBuffer,
                       std::get<1>(m_piplineBind),
                       VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT,
                       offsetof(PushConstants, entityId),
                       sizeof(id),
                       &id);
}

void PBRPrimitiveRenderer::setViewPort(VkViewport vp)
{
    vkCmdSetViewport(m_info.commandBuffer, 0, 1, &vp);
}

void PBRPrimitiveRenderer::setScissor(VkRect2D vp)
{
    vkCmdSetScissor(m_info.commandBuffer, 0, 1, &vp);
}

VkPipelineLayout PBRPrimitiveRenderer::getLayout(VkPipeline p) const
{
    auto layout = std::get<vk::PipelineLayout>(m_storage.getCreateInfo<vkb::ComputePipelineCreateInfo2>(p).layout);
    return layout;
}

VkImageView PBRPrimitiveRenderer::getView(gvk::ObjectID_t<Image> id) const
{
    return get(id)._view;
}

void PBRPrimitiveRenderer::initDSetAllocator()
{
    vkb::DescriptorPoolCreateInfo2 info;

    info.setMaxSets(20)
            .setPoolSize(vk::DescriptorType::eCombinedImageSampler, 5000)
            .setPoolSize(vk::DescriptorType::eUniformBuffer       , 100)
            .setPoolSize(vk::DescriptorType::eStorageBuffer       , 100)
            .setPoolSize(vk::DescriptorType::eStorageBufferDynamic, 100)
            .setPoolSize(vk::DescriptorType::eUniformBufferDynamic, 100)
            .setPoolSize(vk::DescriptorType::eStorageImage, 100)
            .setFlags(   vk::DescriptorPoolCreateFlagBits::eFreeDescriptorSet
                         | vk::DescriptorPoolCreateFlagBits::eUpdateAfterBind);

    m_pipelines.dSetAllocator.init( &m_storage, m_initInfo.device, info);
}

void PBRPrimitiveRenderer::initPipeline()
{
    vkb::GraphicsPipelineCreateInfo2 PCI;

    // Viewports
    PCI.viewportState.viewports.emplace_back( vk::Viewport(0,0,1024,768,0,1.0f));
    PCI.viewportState.scissors.emplace_back(  vk::Rect2D( {0,0}, {1024,768}));

    PCI.dynamicStates.push_back( vk::DynamicState::eViewport);
    PCI.dynamicStates.push_back( vk::DynamicState::eScissor);

    auto vShaderSrc = m_initInfo.resourceLocator->readResourceASCII("shaders/KHR_PBR.vert");
    auto fShaderSrc = m_initInfo.resourceLocator->readResourceASCII("shaders/KHR_PBR.frag");

    auto vShaderSrcSPV = gnl::GLSLCompiler::compileFromFile( m_initInfo.resourceLocator->locate("shaders/KHR_PBR.vert"));// StaticCompiler::compile(vShaderSrc, EShLangVertex);
    auto fShaderSrcSPV = gnl::GLSLCompiler::compileFromFile( m_initInfo.resourceLocator->locate("shaders/KHR_PBR.frag"));;

    // shader stages
    PCI.addStage( vk::ShaderStageFlagBits::eVertex  ,
                  "main",
                  vShaderSrcSPV);
    PCI.addStage( vk::ShaderStageFlagBits::eFragment,
                  "main",
                  fShaderSrcSPV);

    // Render Pass - we will initialize the pipeline by providing the RenderPassCreateInfo2 struct
    //               and have it auto generate the renderpass for us.
    //PCI.renderPass = vkb::RenderPassCreateInfo2::createSimpleRenderPass({{ vk::Format(swapchain), vk::ImageLayout::ePresentSrcKHR}});

    auto layout = _generatePipelineLayoutCreateInfo( {{vShaderSrcSPV, VK_SHADER_STAGE_VERTEX_BIT}, {fShaderSrcSPV, VK_SHADER_STAGE_FRAGMENT_BIT} });
    layout.addPushConstantRange(vk::ShaderStageFlagBits::eVertex | vk::ShaderStageFlagBits::eFragment, 0, 128);
    PCI.layout = layout;

    PCI.rasterizationState.cullMode = vk::CullModeFlagBits::eBack;
    // single attachment (Swapchain), use default values
    // setColorBlendOp and setBlendEnabled are just
    /// being added for show, these are teh default values
    PCI.addBlendStateAttachment().setColorBlendOp(vk::BlendOp::eAdd)
            .setBlendEnable(true);

    PCI.depthStencilState.depthTestEnable=true;
    PCI.depthStencilState.depthWriteEnable=true;

    m_KHR_Pipeline.init(&m_storage, PCI, m_initInfo.device);

    // vertex inputs
    m_KHR_Pipeline.enableVertexAttribute(   0,0, vk::Format::eR32G32B32Sfloat  , 0 ); // position
    m_KHR_Pipeline.enableVertexAttribute(   1,1, vk::Format::eR32G32B32Sfloat  , 0 ); // normal
    m_KHR_Pipeline.enableVertexAttribute(   2,2, vk::Format::eR32G32B32Sfloat  , 0 ); // tangent
    m_KHR_Pipeline.enableVertexAttribute(   3,3, vk::Format::eR32G32Sfloat     , 0 ); // texcoord 0
    m_KHR_Pipeline.enableVertexAttribute(   4,4, vk::Format::eR32G32Sfloat     , 0 ); // texcoord 1
    m_KHR_Pipeline.enableVertexAttribute(   5,5, vk::Format::eR8G8B8A8Unorm    , 0 ); // color
    m_KHR_Pipeline.enableVertexAttribute(   6,6, vk::Format::eR16G16B16A16Uint , 0 ); // joints
    m_KHR_Pipeline.enableVertexAttribute(   7,7, vk::Format::eR32G32B32Sfloat  , 0 ); // weights


    m_KHR_Pipeline.enableVertexInputBinding(0, sizeof( glm::vec3 ), vk::VertexInputRate::eVertex);
    m_KHR_Pipeline.enableVertexInputBinding(1, sizeof( glm::vec3 ), vk::VertexInputRate::eVertex);
    m_KHR_Pipeline.enableVertexInputBinding(2, sizeof( glm::vec3 ), vk::VertexInputRate::eVertex);
    m_KHR_Pipeline.enableVertexInputBinding(3, sizeof( glm::vec2 ), vk::VertexInputRate::eVertex);
    m_KHR_Pipeline.enableVertexInputBinding(4, sizeof( glm::vec2 ), vk::VertexInputRate::eVertex);
    m_KHR_Pipeline.enableVertexInputBinding(5, sizeof( glm::u8vec4 ), vk::VertexInputRate::eVertex);
    m_KHR_Pipeline.enableVertexInputBinding(6, sizeof( glm::u16vec4), vk::VertexInputRate::eVertex);
    m_KHR_Pipeline.enableVertexInputBinding(7, sizeof( glm::vec4 ), vk::VertexInputRate::eVertex);
}

vkb::PipelineLayoutCreateInfo2 PBRPrimitiveRenderer::_generatePipelineLayoutCreateInfo(const std::vector<std::pair<std::vector<uint32_t> &, VkShaderStageFlagBits> > & shaderCode)
{
    vkb::SPIRV_DescriptorSetLayoutGenerator layoutGenerator;
    for(auto & x : shaderCode)
    {
        layoutGenerator.addSPIRVCode( x.first, x.second );
    }
    vkb::PipelineLayoutCreateInfo2 layout;
    layoutGenerator.generate([&](auto & D)
    {
        for(auto & d : D)
        {
            // std::cout << "Set: " << d.first << std::endl;
            auto & Set = layout.newDescriptorSet();
            for(auto & e : d.second)
            {
                std::cout << "binding: " << e.binding << "   count: " << e.descriptorCount << " type: " << to_string(static_cast<vk::DescriptorType>(e.descriptorType)) << std::endl;
                Set.addDescriptor(e.binding, static_cast<vk::DescriptorType>(e.descriptorType), e.descriptorCount, static_cast<vk::ShaderStageFlags>(e.stageFlags));
            }
        }
    });
    return layout;
}

}
